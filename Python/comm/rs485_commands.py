import struct

## PACKET COMMANDS
# General commands
PC_CMD_STATUS          =     0x01
PC_CMD_WHO_AM_I        =     0x02
PC_CMD_DETAILS         =     0x03

# Bootloader commands
PC_CMD_JUMP_TO_MAIN    =     0x10
PC_CMD_ERASE           =     0x11
PC_CMD_FW_START        =     0x12
PC_CMD_FW_DATA         =     0x13
PC_CMD_FW_END          =     0x14
     
# user commands
PC_CMD_JUMP_TO_BOOT    =     0x20

## STATUS RESPONSES
STATUS_IDLE = 0x01
STATUS_BOOTLOADER = 0x02

class ParseException(Exception):
    pass;

class DetailsCmd:
    def __init__(self, sw_major, sw_minor, hw, hw_type, program_valid):
        self.sw_minor = sw_minor
        self.sw_major = sw_major
        self.hw = hw
        self.hw_type = hw_type
        self.program_valid = program_valid

def parse_status_command(packet):
    return ord(packet.data[0]);

def parse_details_command(packet):
    try:
        if len(packet.data) == 4:
            sw_major, sw_minor, hw, hw_type = struct.unpack("<IIII", packet.data);
            return DetailsCmd(sw_major, sw_minor, hw, hw_type, 0)
        else:
            sw_major, sw_minor, hw, hw_type, program_valid = struct.unpack("<IIIII", packet.data);
            return DetailsCmd(sw_major, sw_minor, hw, hw_type, program_valid)
    except Exception as e:
        raise ParseException(str(e))
        
