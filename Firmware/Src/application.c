/* Includes ------------------------------------------------------------------*/
#include "application.h"
#include "rtc.h"

uint8_t flag_jump_to_boot;

/**
 * \brief           Jump to bootloader command received over RS485
 * \param[in]       *pc: Pointer to \ref pc_t structure with packet data
 * \return          Non-zero on success, 0 on failure
 */
static uint8_t
ma_jump_to_boot(const pc_t* pc) {
    uint8_t status = rtc_set_boot();            /* Set flag to backup registers */
    if (status != 0) {
        return 0;                               /* Response with error */
    }
    
    flag_jump_to_boot = 1;                      /* Set flag to jump to boot, actual jump is done in application loop */
    return 1;
}

/**
 * \brief           Initializes main program and prepares commands for communication for main application program
 * \note            Should not be modified. When main init is used for user, use \ref main_userloop function
 *                      declared in user specific application file
 * \retval          0: Application initialized
 * \retval          1: Application not initialized
 */
uint8_t
app_init(void) {
    /* Assign commands for main program */
    pc_cmd_add(PC_CMD_JUMP_TO_BOOT, ma_jump_to_boot);
    
    return 0;
}

/**
 * \brief           Main loop for application
 * \note            Should not be modified. When main loop is used for user, use \ref main_userloop function
 *                      declared in user specific application file
 */
uint8_t
app_loop(void) {
    /**
     * Jump to bootloader is done by writing backup registers to RTC peripheral
     * and then do software jump to bootloader and stay there
     */
    if (flag_jump_to_boot) {                    /* When flag to jump to bootloader is set */
        NVIC_SystemReset();                     /* Perform system reset */
    }
    return 0;
}
