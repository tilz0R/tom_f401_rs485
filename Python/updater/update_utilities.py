import hashlib
import os
import zipfile
import argparse

PASSWORD = b"UcLLh8phBaSnfB85EhkxpkGVLaRtC2bVRwPsx9XS6XMMYgCE7EV8Ajn4juywCv2mQqkFJQgjX8vLbRqJSurXSGMemAmGR9mkbfPtGqnP5WVX78b3ERZuaPJXEDD3vEh2";

def main():

    parser = argparse.ArgumentParser(prog='update_utilties')

    subparsers = parser.add_subparsers(help='commands', dest='command')

    parser_a = subparsers.add_parser('skeleton', help='Create an update skeleton')
    parser_a.add_argument("dir", action='store', help='Directory where to create skeleton')

    # create the parser for the "b" command
    parser_b = subparsers.add_parser('create_file', help='create an update file')
    parser_b.add_argument("dir", action='store', help='directory containing the update files')

    args = parser.parse_args()

    if args.command == 'skeleton':
        
        # check if directory alrady exists
        out_dir = args.dir

        if os.path.isdir(out_dir):
            exit("Directory exists")

        print "Creating directory"
        os.mkdir(out_dir)

        root_dir = os.path.join(out_dir, 'root')
        os.mkdir(root_dir)

    elif args.command == 'create_file':
        
        in_dir = args.dir

        # check if directory alrady exists
        if not os.path.isdir(in_dir):
            exit("Directory does not exists")

        # create a zip of the directory
        out_filename = in_dir + ".bin"
        zipf = zipfile.ZipFile(out_filename, 'w', zipfile.ZIP_DEFLATED)
        zipdir(in_dir, zipf)
        zipf.close()

        # sign the zip file
        h = file_hash(out_filename)

        with open(out_filename, "a") as f:
            f.write(h);

def file_hash(filename):
    h = hashlib.sha256()
    with open(filename, 'rb', buffering=0) as f:
        for b in iter(lambda : f.read(1024), b''):
            h.update(b)

        h.update(PASSWORD)
        return h.hexdigest()

def zipdir(path, ziph):
    # ziph is zipfile handle
    for root, dirs, files in os.walk(path):
        for file in files:
            ziph.write(os.path.join(root, file))


if __name__ == '__main__':
    main();