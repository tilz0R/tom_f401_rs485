#ifndef __USART_H
#define __USART_H

#include "tm_stm32_usart.h"
#include "tm_stm32_usart_dma.h"
    
#define USARTx              USART6
#define USARTx_PP           TM_USART_PinsPack_Custom
#define USARTx_BAUDRATE     115200

void USART_Init(void);
void MX_USART6_UART_Init(void);
void USART_WriteByte(const uint8_t data);
void USART_WriteData(const uint8_t* data, size_t length);
void USART_WriteString(const char* data);

uint8_t USART_ReadByte(uint8_t* byte);

#endif
