/* Includes ------------------------------------------------------------------*/
#include "application.h"
//#include "adc.h"
#include "math.h"
#include "arm_math.h"
#include "stm32f4xx_hal.h"

unsigned char fillFlag = 0;
unsigned char OTFlag = 0;
unsigned char pFlag = 0;

uint32_t lightonFlag = 0, lightonButton = 0, lightonButtonTime = 0;
uint32_t jetonFlag = 0, jetonButton = 0, jetonButtonTime = 0, jetonSwitch = 0;
static uint64_t pump1ChangeTime, pump1ChangeFastFlag =0 , pump1ChangeSlowFlag = 0;
static uint64_t pump2ChangeTime, pump2ChangeFastFlag =0 , pump2ChangeSlowFlag = 0;
static uint64_t tickTime = 0;
static uint64_t pumpStartTime, pumpForceStop;
uint32_t slowON = 0;

ADC_HandleTypeDef hadc1;
q31_t adcResult = 0;

union cvt {
	float val;
	unsigned char b[4];
}x;

/* Filter variable */
arm_fir_instance_q31 avg_filter;
/* USER CODE BEGIN 0 */
#define CNT_OF(x)       (sizeof(x) / sizeof((x)[0]))
#define AVG_FILTER_BLOCK_SIZE      1

q31_t in, out;

/* List of coefficients */
/**
 * Currently they were generated using FDATOOL inside Matlab
 *
 * Open matlab, run "fdatool" from command line:
 *
 * - Filter type: FIR Window
 * - Filter order: 200
 * - Window: Chebyshev
 * - Sampling frequency: 1000 Hz
 * - Cutoff frequency: 10 Hz
 * Exported as signed integers
 *
 * If necessary, tune them to desired value
 */
q31_t coeffs[] = {
             0,        -183,        -504,       -1012,       -1766,       -2836,
         -4302,       -6256,       -8803,      -12059,      -16154,      -21226,
        -27426,      -34914,      -43856,      -54425,      -66795,      -81137,
        -97621,     -116403,     -137626,     -161413,     -187856,     -217017,
       -248914,     -283515,     -320731,     -360407,     -402311,     -446128,
       -491451,     -537768,     -584462,     -630794,     -675904,     -718797,
       -758346,     -793282,     -822192,     -843520,     -855567,     -856492,
       -844317,     -816935,     -772116,     -707518,     -620704,     -509151,
       -370274,     -201442,           0,      236704,      511297,      826349,
       1184348,     1587669,     2038541,     2539016,     3090940,     3695918,
       4355283,     5070068,     5840975,     6668351,     7552157,     8491953,
       9486874,    10535612,    11636411,    12787051,    13984845,    15226644,
      16508834,    17827352,    19177698,    20554953,    21953802,    23368567,
      24793235,    26221500,    27646799,    29062362,    30461258,    31836446,
      33180831,    34487314,    35748856,    36958530,    38109578,    39195473,
      40209970,    41147162,    42001531,    42767999,    43441970,    44019371,
      44496693,    44871021,    45140056,    45302144,    45356284,    45302144,
      45140056,    44871021,    44496693,    44019371,    43441970,    42767999,
      42001531,    41147162,    40209970,    39195473,    38109578,    36958530,
      35748856,    34487314,    33180831,    31836446,    30461258,    29062362,
      27646799,    26221500,    24793235,    23368567,    21953802,    20554953,
      19177698,    17827352,    16508834,    15226644,    13984845,    12787051,
      11636411,    10535612,     9486874,     8491953,     7552157,     6668351,
       5840975,     5070068,     4355283,     3695918,     3090940,     2539016,
       2038541,     1587669,     1184348,      826349,      511297,      236704,
             0,     -201442,     -370274,     -509151,     -620704,     -707518,
       -772116,     -816935,     -844317,     -856492,     -855567,     -843520,
       -822192,     -793282,     -758346,     -718797,     -675904,     -630794,
       -584462,     -537768,     -491451,     -446128,     -402311,     -360407,
       -320731,     -283515,     -248914,     -217017,     -187856,     -161413,
       -137626,     -116403,      -97621,      -81137,      -66795,      -54425,
        -43856,      -34914,      -27426,      -21226,      -16154,      -12059,
         -8803,       -6256,       -4302,       -2836,       -1766,       -1012,
          -504,        -183,           0
};

/* Array of states is long for: number of coeffs + block size for input in filtering + 1 */
q31_t avg_filter_state[CNT_OF(coeffs) + AVG_FILTER_BLOCK_SIZE + 1];

/* Fake input data from ADC for test */
q31_t inputData[100]; 
/* Array for saving output data from filter */
q31_t outputData[CNT_OF(inputData)];

//Pump 1 Commands
uint8_t
app_cb_pump1(const pc_t* pc) {
    switch (pc->data[0]) {          
			case 0: //off
					slowON = 0;
					HAL_GPIO_WritePin(GPIOB, GPIO_PIN_0, GPIO_PIN_RESET);
					HAL_GPIO_WritePin(GPIOB, GPIO_PIN_2, GPIO_PIN_RESET);
					break;
        case 1: //slow
					slowON = 1;
					HAL_GPIO_WritePin(GPIOB, GPIO_PIN_2, GPIO_PIN_RESET);
					pump1ChangeTime = HAL_GetTick();
					pump1ChangeSlowFlag = 1;
					break;
				case 2: //fast
					HAL_GPIO_WritePin(GPIOB, GPIO_PIN_0, GPIO_PIN_RESET);
					pump1ChangeTime = HAL_GetTick();
					pump1ChangeFastFlag = 1;
					break;
    }
    return 1;   
}

//Pump 2 Commands
uint8_t
app_cb_pump2(const pc_t* pc) {
    switch (pc->data[0]) {          /* Read first byte to see direction */
			case 0: //off
					slowON = 0;
					HAL_GPIO_WritePin(GPIOB, GPIO_PIN_1, GPIO_PIN_RESET);
					HAL_GPIO_WritePin(GPIOA, GPIO_PIN_7, GPIO_PIN_RESET);
					break;
        case 1: //slow
					slowON = 1;
					HAL_GPIO_WritePin(GPIOB, GPIO_PIN_1, GPIO_PIN_RESET);
					pump2ChangeTime = HAL_GetTick();
					pump2ChangeSlowFlag = 1;
					break;
				case 2: //fast
					HAL_GPIO_WritePin(GPIOA, GPIO_PIN_7, GPIO_PIN_RESET);
					pump2ChangeTime = HAL_GetTick();
					pump2ChangeFastFlag = 1;
					break;
    }
    return 1; 
}

//					HAL_GPIO_WritePin(GPIOB, GPIO_PIN_1, GPIO_PIN_RESET);
//					HAL_GPIO_WritePin(GPIOA, GPIO_PIN_7, GPIO_PIN_RESET);
//Light Commands
uint8_t
app_cb_lights(const pc_t* pc) {
    switch (pc->data[0]) {          
        case 0: 
					HAL_GPIO_WritePin(GPIOB, GPIO_PIN_15, GPIO_PIN_SET);
					break;
				
        case 1: 
					HAL_GPIO_WritePin(GPIOB, GPIO_PIN_15, GPIO_PIN_RESET);
					break;
    }
    return 1;                       /* Return OK as we succedded */
}

//Heater Commands
uint8_t
app_cb_heaters(const pc_t* pc) {
    switch (pc->data[0]) {          /* Read first byte to see direction */
        case 1: 
					if(pc->data[1]) {					//heater on
						HAL_GPIO_WritePin(GPIOB, GPIO_PIN_8, GPIO_PIN_SET);
					}
					else
					{
						HAL_GPIO_WritePin(GPIOB, GPIO_PIN_8, GPIO_PIN_RESET);
					}
					break;
				
        case 2: 
					if(pc->data[1]) {					//heater on
						HAL_GPIO_WritePin(GPIOB, GPIO_PIN_5, GPIO_PIN_SET);
					}
					else
					{
						HAL_GPIO_WritePin(GPIOB, GPIO_PIN_5, GPIO_PIN_RESET);
					}
					break;
    }
    return 1;                       /* Return OK as we succedded */
}

//Maintenance Relay Commands
uint8_t
app_cb_mainten(const pc_t* pc) {
    switch (pc->data[0]) {          /* Read first byte to see direction */
        case 1: 
					if(pc->data[1]) {					//main 1 on
						HAL_GPIO_WritePin(GPIOA, GPIO_PIN_10, GPIO_PIN_RESET);;
					}
					else
					{
						HAL_GPIO_WritePin(GPIOA, GPIO_PIN_10, GPIO_PIN_SET);
					}
					break;
				
        case 2: 
					if(pc->data[1]) {					//main 2 on
						HAL_GPIO_WritePin(GPIOA, GPIO_PIN_8, GPIO_PIN_RESET);
					}
					else
					{
						HAL_GPIO_WritePin(GPIOA, GPIO_PIN_8, GPIO_PIN_SET);
					}
					break;
    }
    return 1;                       /* Return OK as we succedded */
}

//Dump Valve Commands
uint8_t
app_cb_dump(const pc_t* pc) {
    switch (pc->data[0]) {          /* Read first byte to see direction */
        case 0: 
					HAL_GPIO_WritePin(GPIOC, GPIO_PIN_11, GPIO_PIN_RESET);
					break;
				
        case 1: 
					HAL_GPIO_WritePin(GPIOC, GPIO_PIN_11, GPIO_PIN_SET);
					break;
    }
    return 1;                       /* Return OK as we succedded */
}

//Fill Valve Commands
uint8_t
app_cb_fill(const pc_t* pc) {
    switch (pc->data[0]) {          /* Read first byte to see direction */
        case 0: 
					HAL_GPIO_WritePin(GPIOC, GPIO_PIN_12, GPIO_PIN_RESET);
					break;
				
        case 1: 
					HAL_GPIO_WritePin(GPIOC, GPIO_PIN_12, GPIO_PIN_SET);
					break;
    }
    return 1;                       /* Return OK as we succedded */
}

//Heat Exchanger Commands
uint8_t
app_cb_heatEx(const pc_t* pc) {
    switch (pc->data[0]) {          /* Read first byte to see direction */
        case 0: 
					HAL_GPIO_WritePin(GPIOB, GPIO_PIN_4, GPIO_PIN_RESET);
					break;
				
        case 1: 
					HAL_GPIO_WritePin(GPIOB, GPIO_PIN_4, GPIO_PIN_SET);
					break;
    }
    return 1;                       /* Return OK as we succedded */
}

//Status Commands
uint8_t
app_cb_status(const pc_t* pc) {
    uint8_t detail_data[7];
					
		//x.val = 26.0;
    /* Fill device description */
    detail_data[0] = fillFlag;  
    detail_data[1] = OTFlag;  
    detail_data[2] = pFlag;  
    detail_data[3] = x.b[0]; 
    detail_data[4] = x.b[1]; 
	  detail_data[5] = x.b[2]; 
    detail_data[6] = x.b[3]; 
		
		pc_set_resp_data(detail_data, sizeof(detail_data));
    return 1;                       /* Return OK as we succedded */
}

//lockout release command
uint8_t
app_cb_lockout(const pc_t* pc) {
			switch (pc->data[0]) {          
			case 0: //pressure switch release
				pumpForceStop = 0;
				pumpStartTime = 0;
				OTFlag = 0;
				break;
			
			case 1: //over temperature flag release
				OTFlag = 0;
				break;
			}
    return 1;                       /* Return OK as we succedded */
}

//=============================================
// USER INIT
//=============================================
uint8_t
app_userinit(void) {
	
		int i;
		
		/* Assign commands for application for device type A */;
		/* Initialize all configured peripherals */
		MX_GPIO_Init();
		/* Initialize interrupts */
		MX_NVIC_Init();
		__ADC1_CLK_ENABLE();	
		MX_ADC1_Init();
	
	  /* Init and prepare filter */
    arm_fir_init_q31(&avg_filter, CNT_OF(coeffs), coeffs, avg_filter_state, 1);
    
    /* Process all filters */
    for (i = 0; i < CNT_OF(inputData); i += AVG_FILTER_BLOCK_SIZE) {
        arm_fir_q31(&avg_filter, &inputData[i], &outputData[i], AVG_FILTER_BLOCK_SIZE);
    }
	
		pc_cmd_add(0x81, app_cb_pump1);   //pump1
		pc_cmd_add(0x82, app_cb_pump2);   //pump2
    pc_cmd_add(0x83, app_cb_lights);  //12vac lights
		pc_cmd_add(0x84, app_cb_heaters); //heaters
		pc_cmd_add(0x85, app_cb_mainten); //maintenance
    pc_cmd_add(0x86, app_cb_dump);  	//dump
		pc_cmd_add(0x87, app_cb_fill);   	//fill
		pc_cmd_add(0x88, app_cb_heatEx);  //heat exchanger
    pc_cmd_add(0x89, app_cb_status);  //GET status
		pc_cmd_add(0x90, app_cb_lockout); //RESET PFlag or OTFlag
    
    return 0;
}


//=============================================
// USER LOOP
//=============================================
uint8_t
app_userloop(void) {
		
		HAL_ADC_Start(&hadc1);
		if (HAL_ADC_PollForConversion(&hadc1, 100) == HAL_OK) {
				adcResult = HAL_ADC_GetValue(&hadc1);
		}
    	
    /* Process input data to filter */
    arm_fir_q31(&avg_filter, &adcResult, &out, 1);
		calcTemperature(out);
		
		pFlag = !!HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_1);
		
		if(x.val > 42.0f){
			OTFlag = 1;
		} else {
			/* Temperature check */
			if (x.val < 37.0f && OTFlag == 0) { //if operation is to resume <37 then remove OT test, otherwise ops will only resume once cmd sent to clear OT flag
				if (!pumpForceStop) {
					if (!pumpStartTime) {	//this means if it is 0 then it will be 1 and if any other value than 0 i.e. its some time
						pumpStartTime = HAL_GetTick();
						//Turn pump on to begin circulation
						HAL_GPIO_WritePin(GPIOB, GPIO_PIN_2, GPIO_PIN_RESET);
						pump1ChangeSlowFlag = 1;
					} else { //pump is already running
						if (HAL_GetTick() - pumpStartTime < 10000) {
							//Inside valid 10 seconds
						} else {
							//We are over check time, check pressure
							if (!pFlag) { //if 0 then no pressure stop and lock out pump
								//Stop pump
								HAL_GPIO_WritePin(GPIOB, GPIO_PIN_2, GPIO_PIN_RESET);
								HAL_GPIO_WritePin(GPIOB, GPIO_PIN_0, GPIO_PIN_RESET);
								//heater off
								HAL_GPIO_WritePin(GPIOB, GPIO_PIN_8, GPIO_PIN_RESET);
								pump1ChangeSlowFlag = 0;
								pumpStartTime = 0;
								pumpForceStop = 1;
							}else {							
								//pressure OK apply heater(s)
								HAL_GPIO_WritePin(GPIOB, GPIO_PIN_8, GPIO_PIN_SET);
							}
						}
					}
				}
			} 
		
			if(x.val > 38.0f){
				//heater(s) off
				HAL_GPIO_WritePin(GPIOB, GPIO_PIN_8, GPIO_PIN_RESET);
				//pump(s) off
				HAL_GPIO_WritePin(GPIOB, GPIO_PIN_2, GPIO_PIN_RESET);
				HAL_GPIO_WritePin(GPIOB, GPIO_PIN_0, GPIO_PIN_RESET);
				pump1ChangeSlowFlag = 0;
				pumpStartTime = 0;
			}			

			if(pump1ChangeSlowFlag) {
				if (HAL_GetTick() - pump1ChangeTime > 1000) {
					HAL_GPIO_WritePin(GPIOB, GPIO_PIN_0, GPIO_PIN_SET);
					pump1ChangeSlowFlag = 0;
				}
			}
			
			if(pump1ChangeFastFlag) {
				if (HAL_GetTick() - pump1ChangeTime > 1000) {
					HAL_GPIO_WritePin(GPIOB, GPIO_PIN_2, GPIO_PIN_SET);
					pump1ChangeFastFlag = 0;
				}
			}
			
			if(pump2ChangeSlowFlag) {
				if (HAL_GetTick() - pump2ChangeTime > 1000) {
					HAL_GPIO_WritePin(GPIOA, GPIO_PIN_7, GPIO_PIN_SET);
					pump2ChangeSlowFlag = 0;
				}
			}
			
			if(pump2ChangeFastFlag) {
				if (HAL_GetTick() - pump2ChangeTime > 1000) {
					HAL_GPIO_WritePin(GPIOB, GPIO_PIN_1, GPIO_PIN_SET);
					pump2ChangeFastFlag = 0;
				}
			}
					
			if (HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_4)) {
				if (!lightonButton) {
					lightonButton = 1;
					lightonButtonTime = HAL_GetTick();
				}
			} else {
				if (lightonButton) {
					if (HAL_GetTick() - lightonButtonTime > 50) {
						if (lightonFlag) {
							HAL_GPIO_WritePin(GPIOB, GPIO_PIN_15, GPIO_PIN_RESET);
						} else {
							HAL_GPIO_WritePin(GPIOB, GPIO_PIN_15, GPIO_PIN_SET);
						}
						
						lightonFlag = !lightonFlag;
					}
				}
				lightonButton = 0;
			}
			
			if (HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_5)) {
				if (!jetonButton) {
					jetonButton = 1;
					jetonButtonTime = HAL_GetTick();
				}
			} else {
				if (jetonButton) {
					if (HAL_GetTick() - jetonButtonTime > 50) {
						switch (jetonSwitch % 4) {
							case 0: 
								HAL_GPIO_WritePin(GPIOB, GPIO_PIN_2, GPIO_PIN_RESET);
								pump1ChangeTime = HAL_GetTick();
								pump1ChangeSlowFlag = 1;
								break;
							case 1: 
								HAL_GPIO_WritePin(GPIOB, GPIO_PIN_0, GPIO_PIN_RESET);
								pump1ChangeTime = HAL_GetTick();
								pump1ChangeFastFlag = 1;
								break;
							case 2: 
								HAL_GPIO_WritePin(GPIOB, GPIO_PIN_2, GPIO_PIN_RESET);
								HAL_GPIO_WritePin(GPIOB, GPIO_PIN_0, GPIO_PIN_RESET);
								//heater(s) off
								HAL_GPIO_WritePin(GPIOB, GPIO_PIN_8, GPIO_PIN_RESET);
								pumpStartTime = 0;
								break;
							case 3: 
								break;
						}
						jetonSwitch++;
					}
				}
				jetonButton = 0;
			}
	}
		
  return 0;
}

/* Temp Calc function */
static void calcTemperature(int adcValue)
{
		//calculate heater temperature
		float tempVolt = (adcValue * 0.0008);
		float R2 = 3300.0;
		float Vin = 5.01;
		float R1 = (((Vin*R2) - (tempVolt*R2))/tempVolt);
		float beta = 3435;             				//beta value for the thermistor
		float Rtemp = 25.0 + 273.15;   				//reference temperature (25C)
		float Rresi = 10150;           				//reference resistance at reference temperature - adjust to calibrate
		float Rtherm = R1;
		float y = (Rtherm/Rresi);
		float T = Rtemp * beta / (beta + Rtemp * log(y));
		T = T - 273.15f;         							//convert from Kelvin to Celsius
		x.val = T;
}
/* GPIO init function */
static void MX_GPIO_Init(void)
{
	GPIO_InitTypeDef gInit;
	
  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOA_CLK_ENABLE();
	__HAL_RCC_GPIOB_CLK_ENABLE();
	__HAL_RCC_GPIOC_CLK_ENABLE();
	
	gInit.Pin = GPIO_PIN_0;
	gInit.Mode = GPIO_MODE_ANALOG;
	HAL_GPIO_Init(GPIOC, &gInit);
		
  gInit.Pull = GPIO_NOPULL;
  gInit.Speed = GPIO_SPEED_FAST;
	gInit.Mode = GPIO_MODE_OUTPUT_PP;
	gInit.Pin = GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_4 | GPIO_PIN_5 | GPIO_PIN_8 | GPIO_PIN_15;
	HAL_GPIO_Init(GPIOB, &gInit);
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_0, GPIO_PIN_RESET);							//pump1slow
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_1, GPIO_PIN_RESET);							//pump2trig
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_2, GPIO_PIN_RESET);							//pump1trig
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_4, GPIO_PIN_RESET);							//heat ex relay
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_5, GPIO_PIN_RESET);							//heat 2 trigger
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_8, GPIO_PIN_RESET);							//heat 1 trigger
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_15, GPIO_PIN_RESET);						//new light
	
	gInit.Pin = GPIO_PIN_7 | GPIO_PIN_8 | GPIO_PIN_10;
	HAL_GPIO_Init(GPIOA, &gInit);
	//HAL_GPIO_WritePin(GPIOA, GPIO_PIN_6, GPIO_PIN_RESET);							//light relay
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_7, GPIO_PIN_RESET);							//pump2slow
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_8, GPIO_PIN_SET);							//main 1 relay
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_10, GPIO_PIN_SET);						//main 2 relay
	
	gInit.Pin = GPIO_PIN_6 | GPIO_PIN_11 | GPIO_PIN_12;;
	HAL_GPIO_Init(GPIOC, &gInit);
	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_6, GPIO_PIN_RESET);							//rs485 poll
	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_11, GPIO_PIN_RESET);						//dump relay
	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_12, GPIO_PIN_RESET);						//fill relay
	
	//INPUTS
  /*Configure GPIO pins : PC4 PC5 */
  gInit.Mode = GPIO_MODE_INPUT;
  gInit.Pull = GPIO_PULLDOWN;
	gInit.Pin = GPIO_PIN_4 | GPIO_PIN_5;
	HAL_GPIO_Init(GPIOC, &gInit);
	
	gInit.Pin = GPIO_PIN_1 | GPIO_PIN_2;
	gInit.Mode = GPIO_MODE_IT_RISING_FALLING;
  gInit.Pull = GPIO_PULLDOWN;
  HAL_GPIO_Init(GPIOC, &gInit);
	
	/* Enable and set EXTI Line0 Interrupt to the lowest priority */
  HAL_NVIC_SetPriority(EXTI1_IRQn, 1, 0);
  HAL_NVIC_EnableIRQ(EXTI1_IRQn);
	
	/* Enable and set EXTI Line0 Interrupt to the lowest priority */
//  HAL_NVIC_SetPriority(EXTI2_IRQn, 1, 1);
//  HAL_NVIC_EnableIRQ(EXTI2_IRQn);
	
	HAL_NVIC_SetPriority(EXTI2_IRQn, 1, 1);
  HAL_NVIC_EnableIRQ(EXTI2_IRQn);

}


/* ADC1 init function */
static void MX_ADC1_Init(void)
{
  ADC_ChannelConfTypeDef sConfig;

    /**Configure the global features of the ADC (Clock, Resolution, Data Alignment and number of conversion) 
    */
  hadc1.Instance = ADC1;
  hadc1.Init.ClockPrescaler = ADC_CLOCK_SYNC_PCLK_DIV2;
  hadc1.Init.Resolution = ADC_RESOLUTION_12B;
  hadc1.Init.ScanConvMode = DISABLE;
  hadc1.Init.ContinuousConvMode = DISABLE;
  hadc1.Init.DiscontinuousConvMode = DISABLE;
  hadc1.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
  hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc1.Init.NbrOfConversion = 1;
  hadc1.Init.DMAContinuousRequests = DISABLE;
  hadc1.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
  if (HAL_ADC_Init(&hadc1) != HAL_OK)
  {
    while(1); //Error_Handler(1);
  }

    /**Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time. 
    */
  sConfig.Channel = ADC_CHANNEL_10;
  sConfig.Rank = 1;
  sConfig.SamplingTime = ADC_SAMPLETIME_3CYCLES;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    while(1); //Error_Handler(1);
  }

}



/* NVIC Configuration */
static void MX_NVIC_Init(void)
{
  /* ADC_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(ADC_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(ADC_IRQn);
}

//pressure switch
void EXTI1_IRQHandler(void) {
	HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_1);
	pFlag = !!HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_1);
	pumpStartTime = 0;
}

////OT Switch
//void EXTI2_IRQHandler(void) {
//	HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_2);
//	OTFlag = !!HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_2);
//}

//Fill switch
void EXTI2_IRQHandler(void) {
  HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_2);
	fillFlag = !!HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_2);
}






