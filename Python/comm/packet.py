import crcmod.predefined
import struct
import flog

START_BYTE = 0xAA
STOP_BYTE = 0x55

crc16_func = crcmod.predefined.mkCrcFun('crc-16')

def hex_dump(address_from, address_to, cmd, data, crc):

    if not data:
        data = []
    out = hex(address_from) + " " + \
            hex(address_to) + " " + \
            hex(cmd) + " " + \
            hex(len(data))

    for c in data:
        out += " " + c

    out += " " + hex(crc)
           

    return out

class PacketDecode:
    def __init__(self):

        self._state_data = b'';
        self._state_data_length = 0;
        self._state_position = 0;
        self._state_address_from = 0;
        self._state_address_to = 0;
        self._state_cmd = 0;
        self._state_crc = 0;
        self._state_crc_counter = 0;
        

    def reset_stream(self):
        self._state_position = 0;

    def decode_stream(self, stream):

        packets = [];
        
        for character in stream:
            c = ord(character);
            if self._state_position == 0:
                if c != START_BYTE:
                    pass;
                else:
                    self._state_position = 1;

                    self._state_crc_counter = 0;
                    self._state_data = b'';

            elif self._state_position == 1:
                self._state_address_from = c;
                self._state_position = 2;
            
            elif self._state_position == 2:
                self._state_address_to = c;
                self._state_position = 3;
      
            elif self._state_position == 3:
                self._state_cmd = c;
                self._state_position = 4;

            elif self._state_position == 4:
                self._state_data_length = c;
                self._state_position = 5;
                if (self._state_data_length == 0):
                    self._state_crc = 0;
                    self._state_position = 6;

            elif self._state_position == 5:
                if len(self._state_data) < self._state_data_length:
                    self._state_data += character;

                if len(self._state_data) >= self._state_data_length:
                    self._state_position = 6;
                    self._state_crc = 0;

            elif self._state_position == 6:

                self._state_crc = (self._state_crc >> 8) | (c << 8)
                self._state_crc_counter += 1;
                if (self._state_crc_counter == 2):
                    self._state_position = 7;

            elif self._state_position == 7:

                err = 0;

                # validate crc
                crc_computed = crc16_func(chr(self._state_address_from) + \
                                            chr(self._state_address_to) + \
                                            chr(self._state_cmd) + \
                                            chr(len(self._state_data)) + \
                                            self._state_data)

                if crc_computed != self._state_crc:
                    flog.flog_warn("decode_stream(): CRC missmatch {0}".format(hex_dump(self._state_address_from, self._state_address_to, self._state_cmd, self._state_data, self._state_crc, crc_computed)))
                    err = -1

                if c != STOP_BYTE:
                    flog.flog_warn("decode_stream(): STOP byte missmatch")
                    err = -2

                if err == 0:
                    packets.append(Packet(self._state_address_from, self._state_address_to, self._state_cmd, self._state_data))

                self._state_position = 0;

        return packets; 

def PacketEncode(address_from, address_to, cmd, data):
    comm_len = 0

    bytes_out = chr(START_BYTE)

    if data != None:
        comm_len = len(data);
    
    crc_data = struct.pack('BBBB', int(address_from), int(address_to), int(cmd), comm_len);

    if data != None:
        crc_data += data;

    crc_value = crc16_func(crc_data)

    bytes_out += crc_data + struct.pack('<H', crc_value);

    bytes_out += chr(STOP_BYTE);

    return bytes_out

class Packet:
    def __init__(self, address_from, address_to, cmd, data):
        self.data = data;
        self.address_from = address_from;
        self.address_to = address_to;
        self.cmd = cmd;