import xml.etree.ElementTree as ET

import collections

from sched.arguments import ArgumentException, Argument


class OperationsParseException(Exception):
    pass


class InvalidTypeException(Exception):
    pass


class OperationPlan:
    def __init__(self):
        self._plan = []
        self._systems = []

        for system in range(1, 9):
            self._systems.append(System(system))

    def get_by_id(self, id):
        for system in self._systems:
            if system.get_id() == int(id):
                return system

    def add_operation(self, operation, arguments):
        self._plan.append(
            {
                "operation": operation,
                "args": arguments
            }
        )

    def enable_system(self, id):
        for sys in self._systems:
            if sys.get_id() == id:
                sys.enable()

    def get_enabled_systems(self):
        enabled = []
        for sys in self._systems:
            if sys.is_enabled():
                enabled.append(sys)
        return enabled

    def get_operations(self):
        return self._plan

    def parse_xml(self, operations, file):
        tree = ET.parse(file)
        root = tree.getroot()

        all_operations = operations.get_operations()
        for child in root:

            # config
            if child.tag == "config":
                systems = child[0]

                for system in systems:
                    if 'id' in system.attrib:
                        id = system.attrib['id']
                    else:
                        raise OperationsParseException("invalid system tag specification - missing id attribute")

                    try:
                        id = int(id)
                        if system.text.lower() == "on" or \
                                        system.text == "1":
                            self.enable_system(id)
                    except ValueError as e:
                        raise OperationsParseException("invalid system tag specification - id not integer")


            # scheduler
            elif child.tag == "scheduler":
                for operation in child:
                    found_operation = None
                    arguments = []
                    for config in operation:
                        if config.tag == "name":
                            # loop all operations and see if name fits
                            for op in all_operations:
                                if op == config.text:
                                    # found it
                                    found_operation = all_operations[op]

                        if config.tag == "arguments":
                            for arg in config:
                                if 'name' in arg.attrib:
                                    name = arg.attrib['name']
                                else:
                                    raise OperationsParseException(
                                        "invalid argument tag specification - missing name attribute")
                                arguments.append([name, arg.text])

                    if found_operation is None:
                        raise OperationsParseException("invalid operation name")

                    possible_args = found_operation.get_arguments()

                    possible_args = self._check_arguments(possible_args)

                    for possible_arg in possible_args:
                        found = None
                        for read_arg in arguments:
                            if read_arg[0] == possible_arg.get_name():
                                found = read_arg
                                break

                        if found == None:
                            raise OperationsParseException("Missing required argument %s" % (possible_arg.get_name(),))

                        try:
                            possible_arg.set_value(found[1])
                        except ArgumentException as e:
                            raise OperationsParseException("Argument error: %s" % (e))

                    self.add_operation(found_operation, possible_args)

    def _check_arguments(self, arguments):
        if arguments == None:
            arguments = []

            # String is also collection this is why we check for that
        if type(arguments) is str:
            raise OperationsParseException("Arguments type error")

            # Should be list by now
        if not isinstance(arguments, collections.Sequence):
            # Create if necessary
            if isinstance(arguments, Argument):
                arguments = [arguments]
            else:
                # Dead end
                raise OperationsParseException("Arguments type error")

        return arguments


class System:
    def __init__(self, id):
        self._id = id
        self._addresses = {
            "A": id * 4 + 1,
            "B": id * 4 + 2,
            "C": id * 4 + 3,
            "D": id * 4 + 4,
        }

        self._enabled = False

    def enable(self):
        self._enabled = True

    def is_enabled(self):
        return self._enabled

    def get_address(self, type):
        # Do some error checking
        if not isinstance(type, str):
            raise InvalidTypeException("Controller name must be string")

        if type in self._addresses:
            return self._addresses[type]

        raise InvalidTypeException("No controller with that name: " + type)

    def get_id(self):
        return self._id