import threading

from operations import OperationRuntimeException


class OperationScheduler(threading.Thread):

    def __init__(self, system, operations, error_callback, progress_callback):
        threading.Thread.__init__(self)

        self._system = system
        self._operations = operations
        self._error_cb = error_callback
        self._progress_cb = progress_callback

        self._error = False
        self._error_event = threading.Event()

        self._running = False

    def run(self):
        self._running = True
        self._continue_on_error = False

        while self._running:
            progress = 0
            if self._progress_cb is not None:
                self._progress_cb(self, "start", 0)

            try:
                for operation in self._operations:
                    # execute operation
                    success = self._operations[progress]["operation"].execute(self._operations[progress]["args"], self._system)

                    if success is not True:
                        raise OperationRuntimeException("Returned False")

                    # post progress to UI
                    progress += 1
                    if self._progress_cb is not None:
                        self._progress_cb(self, "progress", float(progress) / len(self._operations))

                if self._progress_cb is not None:
                    self._progress_cb(self, "finish", 0)
            except Exception as e:
                self._continue_on_error = False

                # log exception
                if self._error_cb is not None:
                    self._error_cb(self, e)

                # wait for error to be reset
                print "wait for event"
                self._error_event.clear()
                self._error_event.wait()
                print "event cleared"

    def _reset_error(self):
        self._error_event.set()

    def restart_operation(self):
        print "restart"
        self._continue_on_error = False
        self._reset_error()

    def continue_operation(self):
        print "continue"
        self._continue_on_error = True
        self._reset_error()

    def stop(self):
        self._running = False
        self.join()


class OperationSchedulerController:
    def __init__(self, plan, error_cb = None, progress_cb = None):
        self._plan = plan
        self._threads = []
        self._error_cb = error_cb
        self._progress_cb = progress_cb

    def start(self):

        # Loop through system and enable them
        for sys in self._plan.get_enabled_systems():

            # start a thread for each system
            t = OperationScheduler(
                sys,
                self._plan.get_operations(),
                self._scheduler_error_callback,
                self._scheduler_progress_callback
            )

            t.daemon = True

            # add to list
            self._threads.append([t, sys])

            # start the thread
            t.start()

    def _scheduler_error_callback(self, system, exception):
        sys = None
        for t in self._threads:
            if t[0] == system:
                sys = t[1]

        if self._error_cb is not None and sys is not None:
            self._error_cb(sys, exception)

    def _scheduler_progress_callback(self, system, state, progress):
        sys = None
        for t in self._threads:
            if t[0] == system:
                sys = t[1]

        if self._progress_cb is not None and sys is not None:
            self._progress_cb(sys, state, progress)

    def stop(self):
        for t in self._threads:
            t[0].stop()

    def restart_operation(self, sys):
        for t in self._threads:
            if t[1].get_id() == sys.get_id():
                t[0].restart_operation()

    def continue_operation(self, sys):
        for t in self._threads:
            if t[1].get_id() == sys.get_id():
                t[0].continue_operation()
