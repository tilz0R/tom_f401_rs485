import threading
import time
from packet import Packet, PacketDecode, PacketEncode
import flog
import serial
import struct;
import traceback

ADDRESS_BROADCAST = 0xFF
ADDRESS_MASTER = 0x01

class NoResponseException(Exception):
    pass


class ErrorPacketException(Exception):
    def __init__(self, packet):
        self.packet = packet


class SerialController(threading.Thread):
    STATE_CONNECTED = 1
    STATE_DISCONNECTED = 2

    def __init__(self, port, baud, strategy, retries):

        threading.Thread.__init__(self)
        self.daemon = True

        self._running = False
        self._conn = False

        self._port = port
        self._baud = baud

        self._callbacks = {}
        self.state_callback = None

        self._packet_decoder = PacketDecode()

        self._rs485_strategy = strategy
        self._rs485_retries = retries

        self._event = threading.Event()

        self._packets = None

        if self._rs485_strategy == 'gpio':
            
            import RPi.GPIO as GPIO
            GPIO.setmode(GPIO.BCM)
            GPIO.setup(18, GPIO.OUT)
            GPIO.output(18, GPIO.LOW)

    def run(self):
        self._running = True
        while self._running:
            if self._conn:
                try:
                    d = self._conn.read(10)

                    packets = self._packet_decoder.decode_stream(d)

                    if packets:
                        self._packets = packets
                        self._event.set()
                except Exception as e:
                    traceback.print_exc()
                    if self.state_callback:
                        self.state_callback(SerialController.STATE_DISCONNECTED)
                    self._conn = False
            else:
                time.sleep(1)
                self.open_serial()
                if self._conn:
                    if self.state_callback:
                        self.state_callback(SerialController.STATE_CONNECTED)

    def send_receive(self, address_to, command, data = None, broadcast = False):
        if not self._conn:
            raise NoResponseException()

        if broadcast:
            address_from = ADDRESS_BROADCAST
        else:
            address_from = ADDRESS_MASTER

        self._packets = None

        tries = 0
        while tries < self._rs485_retries:

            try:
                # clear event
                self._event.clear()
                
                # reset stream decoder state machine
                self._packet_decoder.reset_stream()

                # send packet
                sent = self._send_packet(address_from, address_to, command, data)
                if not sent:
                    flog.flog_warn("send_receive(): send_packet error. CMD - {0}".format(command))
                    tries += 1
                    continue

                # wati for response
                event_rcv = self._event.wait(1)
                if not event_rcv:

                    # we dont expect response to broadcast
                    if address_from == ADDRESS_BROADCAST:
                        break

                    flog.flog_warn("send_receive(): Packet rcv timeout. CMD - {0}".format(hex(command)))
                    tries += 1
                    continue

                # packet received
                break

            except Exception as e:
                tries += 1
                flog.flog_warn("send_receive(): Packet rcv exception. CMD - {0}".format(hex(command)))

        if tries == self._rs485_retries:
            # error
            flog.flog_error("send_receive(): Maximum retransmits reached. CMD - {0}".format(hex(command)))
            raise NoResponseException()

        if self._packets and len(self._packets) > 0:
            flog.flog_debug("send_receive(): successful. CMD - {0}, RSP - {1}".format(hex(command), hex(self._packets[0].cmd)))

            p = self._packets[0]
            if p.cmd == command:
                return p

            raise ErrorPacketException(p)
            return self._packets[0]

        # This is ok for broadcast message
        if address_from == ADDRESS_BROADCAST:
            return True

        flog.flog_error("send_receive(): Unknown error. CMD - {0}".format(command))
        return False


    def _send_packet(self, address_from, address_to, command, data = None):
        if self._conn:

            bytes_out = PacketEncode(address_from, address_to, command, data)

            if self._rs485_strategy == 'gpio':
                GPIO.output(18, GPIO.HIGH)

            if self._rs485_strategy == 'dtr':
                self._conn.dtr = False

            time.sleep(0.001)    
            
            self._conn.write(bytes_out)
            self._conn.flush()

            time.sleep(0.001)

            if self._rs485_strategy == 'gpio':
                GPIO.output(18, GPIO.LOW)

            if self._rs485_strategy == 'dtr':
                self._conn.dtr = True

            return True
        else:
            return False

    def is_connected(self):
        return self._conn

    def set_state_callback(self, callback):
        self.state_callback = callback

    def connect(self):

        if self._running == False:
            self.start()

        return True

    def open_serial(self):
        if self._conn:
            try:
                self._conn.close()
            except Exception as e:
                pass

            self._conn = False

        try:
            if self._rs485_strategy == 'dtr':

                c = serial.Serial(None, self._baud)
                c.dtr = True
                c.port = self._port
                c.open()

                self._conn = c
            else:
                self._conn = serial.Serial(self._port, self._baud)
            self._conn.timeout = 0.01

        except Exception as e:
            return False

        return True

    def add_command_callback(self, command, callback):
        if command in self._callbacks:
            self._callbacks[command].append(callback);
        else:
            self._callbacks[command] = [callback];


