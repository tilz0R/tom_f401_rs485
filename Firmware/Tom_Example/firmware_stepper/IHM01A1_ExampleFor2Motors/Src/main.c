/**
  ******************************************************************************
  * @file    Multi/Examples/MotionControl/IHM01A1_ExampleFor2Motors/Src/main.c 
  * @author  IPC Rennes
  * @version V1.7.0
  * @date    August 11th, 2016  
  * @brief   This example shows how to drive 2 motors with 2 IHM01A1 expansion boards
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT(c) 2014 STMicroelectronics</center></h2>
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "usart.h"
#include "packet.h"
    
/** @defgroup IHM01A1_Example_for_2_motor_devices
  * @{
  */ 

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
static volatile uint16_t gLastError;
int32_t pos;
uint32_t myMaxSpeed;
uint32_t myMinSpeed;
uint16_t myAcceleration;
uint16_t myDeceleration;
  
/* Private function prototypes -----------------------------------------------*/
static void MyFlagInterruptHandler(void);
static void init_motor(void);
static void MX_GPIO_Init(void);

/* Private functions ---------------------------------------------------------*/

#define DEVICE_ADDRESS 0x02

/* Setup EXTI pins here */
#define EXTI1_PORT      GPIOA
#define EXTI1_PIN       GPIO_PIN_0
#define EXTI2_PORT      GPIOA
#define EXTI2_PIN       GPIO_PIN_1

int port = 0;
int portIdx = 0;
    
/**
  * @brief  Main program
  * @param  None
  * @retval None
  */
int main(void) {
    packet_t* packet;
    
    /* STM32xx HAL library initialization */
    HAL_Init();

    /* Configure the system clock */
    SystemClock_Config();
	
		GPIO_InitTypeDef gInit;

		/* GPIO Ports Clock Enable */
		__HAL_RCC_GPIOB_CLK_ENABLE();
		
		gInit.Mode = GPIO_MODE_OUTPUT_PP;
		gInit.Pin = GPIO_PIN_0;
		HAL_GPIO_Init(GPIOB, &gInit);
		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_0, GPIO_PIN_RESET);		//RS485 RX mode

    //Motor is not connected
    //Uncomment when motor is there otherwise it will hang
    init_motor();
    
    USART_Init();
    PACKET_Init();
		
		MX_GPIO_Init();
    
    //USART_WriteString("START\n");
    
    /* Infinite loop */
    while (1) {
			
				//HAL_GPIO_TogglePin(GPIOB, GPIO_PIN_0);
				//HAL_Delay(1000);
			
        if ((packet = PACKET_Read()) != NULL) {
            /* Is packet valid? */
            if (packet->status == PACKET_STATUS_OK) {
                if (packet->address == DEVICE_ADDRESS) {
                    //PACKET_Write(packet);
									//USART_WriteString("PACKET Rx'd\n");
									switch (packet->data[0])
									{
										// E.STOP
										case 0:
										{
											HAL_GPIO_WritePin(GPIOB, GPIO_PIN_0, GPIO_PIN_SET);		//RS485 TX mode
											HAL_Delay(10);
											USART_WriteByte(0x55);
											HAL_Delay(10);
											HAL_GPIO_WritePin(GPIOB, GPIO_PIN_0, GPIO_PIN_RESET);		//RS485 TX mode
											
											BSP_MotorControl_HardStop(0);
											//BSP_MotorControl_HardStop(1);

											HAL_GPIO_WritePin(GPIOB, GPIO_PIN_0, GPIO_PIN_SET);		//RS485 TX mode
											HAL_Delay(10);
											USART_WriteByte(0x22);
											HAL_Delay(10);
											HAL_GPIO_WritePin(GPIOB, GPIO_PIN_0, GPIO_PIN_RESET);		//RS485 TX mode
											break;
										}
										
										//SET SPEED
										case 1:
										{
											HAL_GPIO_WritePin(GPIOB, GPIO_PIN_0, GPIO_PIN_SET);		//RS485 TX mode
											HAL_Delay(10);
											USART_WriteByte(0x44);
											USART_WriteByte(0x33);
											HAL_Delay(10);
											HAL_GPIO_WritePin(GPIOB, GPIO_PIN_0, GPIO_PIN_RESET);		//RS485 TX mode
											
											BSP_MotorControl_SetMaxSpeed(0, ((packet->data[1]<<8)+ packet->data[2]));
											//BSP_MotorControl_SetMaxSpeed(1, ((packet->data[1]<<8)+ packet->data[2]));				
											

											HAL_GPIO_WritePin(GPIOB, GPIO_PIN_0, GPIO_PIN_SET);		//RS485 TX mode
											HAL_Delay(10);
											USART_WriteByte(0x22);
											HAL_Delay(10);
											HAL_GPIO_WritePin(GPIOB, GPIO_PIN_0, GPIO_PIN_RESET);		//RS485 TX mode											
											
											break;
										}	
										
										//MOVE FWD/BACKWARDS for a distance
										case 2:
										{
											HAL_GPIO_WritePin(GPIOB, GPIO_PIN_0, GPIO_PIN_SET);		//RS485 TX mode
											HAL_Delay(10);
											USART_WriteByte(0x44);
											USART_WriteByte(0x33);
											HAL_Delay(10);
											HAL_GPIO_WritePin(GPIOB, GPIO_PIN_0, GPIO_PIN_RESET);		//RS485 TX mode
											
//										HAL_NVIC_SetPriority(EXTI1_IRQn, 0, 0);
//										HAL_NVIC_EnableIRQ(EXTI1_IRQn);
//											
//										port = packet->data[2];
											if(packet->data[1] == 1){
												//BSP_MotorControl_Run(0,BACKWARD);
//												BSP_MotorControl_CmdEnable(0);
//												BSP_MotorControl_CmdEnable(1);
												BSP_MotorControl_Move(0, BACKWARD, ((packet->data[2]<<8)+packet->data[3])); 
												//BSP_MotorControl_Move(1, BACKWARD, ((packet->data[2]<<8)+packet->data[3])); 
//												BSP_MotorControl_CmdDisable(0);
//												BSP_MotorControl_CmdDisable(1);
											}else
											{
												//BSP_MotorControl_Run(0,FORWARD);
//												BSP_MotorControl_CmdEnable(0);
//												BSP_MotorControl_CmdEnable(1);
												BSP_MotorControl_Move(0, FORWARD, ((packet->data[2]<<8)+packet->data[3]));
												//BSP_MotorControl_Move(1, FORWARD, ((packet->data[2]<<8)+packet->data[3]));
//												BSP_MotorControl_CmdDisable(0);
//												BSP_MotorControl_CmdDisable(1);
											}

											HAL_GPIO_WritePin(GPIOB, GPIO_PIN_0, GPIO_PIN_SET);		//RS485 TX mode
											HAL_Delay(10);
											USART_WriteByte(0x22);
											HAL_Delay(10);
											HAL_GPIO_WritePin(GPIOB, GPIO_PIN_0, GPIO_PIN_RESET);		//RS485 TX mode
											break;
										}

									}
                } else {
                    //USART_WriteString("PACKET IS NOT FOR US\n");
                }
            } else {
                //USART_WriteString("PACKET INVALID\n");
            }
        } else {
            //There is no packet for read, maybe it is not yet filled to the end
        }
    }
}

static void MX_GPIO_Init(void)
{

  GPIO_InitTypeDef GPIO_InitStruct;

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOA_CLK_ENABLE();

  /*Configure GPIO pins : PA0 PA1 */
  GPIO_InitStruct.Pin = GPIO_PIN_0|GPIO_PIN_1;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

}

void EXTI0_IRQHandler(void)
{
  /* Home Position */
	BSP_MotorControl_HardStop(0);
	//HAL_NVIC_DisableIRQ(EXTI0_IRQn);
  HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_0);
}

/**
* @brief This function handles EXTI line1 interrupt.
*/
void EXTI1_IRQHandler(void)
{
	if(portIdx == (port*2)){
		BSP_MotorControl_HardStop(0);
		BSP_MotorControl_Move(0, BACKWARD, 24);
		portIdx = 0;
		HAL_NVIC_DisableIRQ(EXTI1_IRQn);
	}else
	{
		portIdx++;
	}
	
	HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_1);	
  
}

/* USART Custom pins callback function */
void TM_USART_InitCustomPinsCallback(USART_TypeDef* USART_Param, uint16_t AlternateFunction) { 
	/* Check for proper USART */
	if (USART_Param == USART6) {
		/* Init pins for USART 6 */
		TM_GPIO_InitAlternate(GPIOA, GPIO_PIN_11 | GPIO_PIN_12, TM_GPIO_OType_PP, TM_GPIO_PuPd_UP, TM_GPIO_Speed_Fast, AlternateFunction);
	}
}

/**
  * @brief  This function is the User handler for the flag interrupt
  * @param  None
  * @retval None
  */
void MyFlagInterruptHandler(void)
{
  /* Get status of device 0 */
  /* this will clear the flags */
  uint16_t statusRegister = BSP_MotorControl_CmdGetStatus(0);

  /* Check HIZ flag: if set, power brigdes are disabled */
  if ((statusRegister & L6474_STATUS_HIZ) == L6474_STATUS_HIZ)
  {
    // HIZ state
    // Action to be customized
  }

  /* Check direction bit */
  if ((statusRegister & L6474_STATUS_DIR) == L6474_STATUS_DIR)
  {
    // Forward direction is set
    // Action to be customized    
  }  
  else
  {
    // Backward direction is set
    // Action to be customized    
  }  

  /* Check NOTPERF_CMD flag: if set, the command received by SPI can't be performed */
  /* This often occures when a command is sent to the L6474 */
  /* while it is in HIZ state */
  if ((statusRegister & L6474_STATUS_NOTPERF_CMD) == L6474_STATUS_NOTPERF_CMD)
  {
       // Command received by SPI can't be performed
       // Action to be customized    
  }  

  /* Check WRONG_CMD flag: if set, the command does not exist */
  if ((statusRegister & L6474_STATUS_WRONG_CMD) == L6474_STATUS_WRONG_CMD)
  {
     //command received by SPI does not exist 
     // Action to be customized        
  }  

  /* Check UVLO flag: if not set, there is an undervoltage lock-out */
  if ((statusRegister & L6474_STATUS_UVLO) == 0)
  {
     //undervoltage lock-out 
     // Action to be customized            
  }  

  /* Check TH_WRN flag: if not set, the thermal warning threshold is reached */
  if ((statusRegister & L6474_STATUS_TH_WRN) == 0)
  {
    //thermal warning threshold is reached
    // Action to be customized            
  }    

  /* Check TH_SHD flag: if not set, the thermal shut down threshold is reached */
  if ((statusRegister & L6474_STATUS_TH_SD) == 0)
  {
    //thermal shut down threshold is reached 
    // Action to be customized            
  }    

  /* Check OCD  flag: if not set, there is an overcurrent detection */
  if ((statusRegister & L6474_STATUS_OCD) == 0)
  {
    //overcurrent detection 
    // Action to be customized            
  }      

  /* Get status of device 1 */
  /* this will clear the flags */
  statusRegister = BSP_MotorControl_CmdGetStatus(1);  

  /* Check HIZ flag: if set, power brigdes are disabled */
  if ((statusRegister & L6474_STATUS_HIZ) == L6474_STATUS_HIZ)
  {
    // HIZ state
    // Action to be customized            
  }

  /* Check direction bit */
  if ((statusRegister & L6474_STATUS_DIR) == L6474_STATUS_DIR)
  {
    // Forward direction is set
    // Action to be customized            
  }  
  else
  {
    // Backward direction is set
    // Action to be customized            
  }  

  /* Check NOTPERF_CMD flag: if set, the command received by SPI can't be performed */
  /* This often occures when a command is sent to the L6474 */
  /* while it is in HIZ state */
  if ((statusRegister & L6474_STATUS_NOTPERF_CMD) == L6474_STATUS_NOTPERF_CMD)
  {
      // Command received by SPI can't be performed
     // Action to be customized            
  }  

  /* Check WRONG_CMD flag: if set, the command does not exist */
  if ((statusRegister & L6474_STATUS_WRONG_CMD) == L6474_STATUS_WRONG_CMD)
  {
     //command received by SPI does not exist 
     // Action to be customized          
  }  

  /* Check UVLO flag: if not set, there is an undervoltage lock-out */
  if ((statusRegister & L6474_STATUS_UVLO) == 0)
  {
     //undervoltage lock-out 
     // Action to be customized          
  }  

  /* Check TH_WRN flag: if not set, the thermal warning threshold is reached */
  if ((statusRegister & L6474_STATUS_TH_WRN) == 0)
  {
    //thermal warning threshold is reached
    // Action to be customized          
  }    

  /* Check TH_SHD flag: if not set, the thermal shut down threshold is reached */
  if ((statusRegister & L6474_STATUS_TH_SD) == 0)
  {
    //thermal shut down threshold is reached 
    // Action to be customized          
  }    

  /* Check OCD  flag: if not set, there is an overcurrent detection */
  if ((statusRegister & L6474_STATUS_OCD) == 0)
  {
    //overcurrent detection 
    // Action to be customized          
  }      

}

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  error number of the error
  * @retval None
  */
void Error_Handler(uint16_t error)
{
  /* Backup error number */
  gLastError = error;
  
  /* Infinite loop */
  while(1)
  {
  }
}

static void init_motor(void) {
/* Set the L6474 library to use 2 device */
  BSP_MotorControl_SetNbDevices(BSP_MOTOR_CONTROL_BOARD_ID_L6474, 1);
  
  /* When BSP_MotorControl_Init is called with NULL pointer,                  */
  /* the L6474 registers and parameters are set with the predefined values from file   */
  /* l6474_target_config.h, otherwise the registers are set using the   */
  /* L6474_Init_t pointer structure                */
  /* The first call to BSP_MotorControl_Init initializes the first device     */
  /* whose Id is 0.                                                           */
  /* The nth call to BSP_MotorControl_Init initializes the nth device         */
  /* whose Id is n-1.                                                         */
  
  /* Initialisation of first device */
  BSP_MotorControl_Init(BSP_MOTOR_CONTROL_BOARD_ID_L6474, NULL);
  /* Initialisation of second device */
  //BSP_MotorControl_Init(BSP_MOTOR_CONTROL_BOARD_ID_L6474, NULL);
  
  
  /* Attach the function MyFlagInterruptHandler (defined below) to the flag interrupt */
  BSP_MotorControl_AttachFlagInterrupt(MyFlagInterruptHandler);

  /* Attach the function Error_Handler (defined below) to the error Handler*/
  BSP_MotorControl_AttachErrorHandler(Error_Handler);
  
  /* Set Current position to be the home position for device 0 */
  //BSP_MotorControl_SetHome(0);

  /* Set Current position to be the home position for device 1 */
  //BSP_MotorControl_SetHome(1);

  /* Request device 0 to Goto position 3200 */
  //BSP_MotorControl_GoTo(0,3200);  
   
  /* Wait for device 0 ends moving */  
  //BSP_MotorControl_WaitWhileActive(0);

  /* Get current position of device 0*/
  //pos = BSP_MotorControl_GetPosition(0);
  
  /* If the read position of device 0 is 3200 */
  /* Request device 1 to go to the same position */
  //if (pos == 3200)
  //{
//    BSP_MotorControl_GoTo(1,pos); 
//    /* Wait for  device 1 ends moving */  
//    BSP_MotorControl_WaitWhileActive(1);
//  }
//  
//  /* Wait for 2 seconds */
//  HAL_Delay(2000);  
//  
//  /* Set current position of device 1 to be its mark position*/
//  BSP_MotorControl_SetMark(1); 
//  
// /* Request device 1 to Goto position -3200 */
//  BSP_MotorControl_GoTo(1,-3200);  
//   
//  /* Wait for device 1 ends moving */  
//  BSP_MotorControl_WaitWhileActive(1);

//  /* Get current position of device 1*/
//  pos = BSP_MotorControl_GetPosition(1);
//  
//  /* If the read position of device 1 is -3200 */
//  /* Request device 1 to go to the same position */
//  if (pos == -3200)
//  {
//    BSP_MotorControl_GoTo(0,pos); 
//    /* Wait for  device 1 ends moving */  
//    BSP_MotorControl_WaitWhileActive(0);
//  }

//  /* Wait for 2 seconds */
//  HAL_Delay(2000);  
//  
//  /* Set current position of device 1 to be its mark position*/
//  BSP_MotorControl_SetMark(0); 
// 
//  /* Request both devices to go home */
//  BSP_MotorControl_GoHome(0);  
//  BSP_MotorControl_GoHome(1);  

//  /* Wait for both devices end moving */  
//  BSP_MotorControl_WaitWhileActive(0);
//  BSP_MotorControl_WaitWhileActive(1);

//  /* Wait for 2 seconds */
//  HAL_Delay(2000);  

// /* Request both devices to go to their mark position */
//  BSP_MotorControl_GoMark(0);  
//  BSP_MotorControl_GoMark(1);  

//  /* Wait for both devices end moving */  
//  BSP_MotorControl_WaitWhileActive(0);
//  BSP_MotorControl_WaitWhileActive(1);

//  /* Wait for 2 seconds */
//  HAL_Delay(2000);  

//  /* Request device 0 to run in FORWARD direction */
//  BSP_MotorControl_Run(0, FORWARD);
//  
//  /* Get device 0 max speed */
//  myMaxSpeed = BSP_MotorControl_GetMaxSpeed(0);
//  
//  /* Wait for device 0 reaches its max speed */
//  while (BSP_MotorControl_GetCurrentSpeed(0) != myMaxSpeed);

//  /* Set max speed of device 1 to be half of the one of device 0 */
//  BSP_MotorControl_SetMaxSpeed(1, myMaxSpeed / 2);

//  /* Request device 1 to run in backward direction*/
//  BSP_MotorControl_Run(1, FORWARD);
//  
//  /* Wait for device 1 reaches its max speed */
//  while (BSP_MotorControl_GetCurrentSpeed(1) != (myMaxSpeed / 2));
//  
//  /* Wait for 5 seconds */
//  HAL_Delay(5000);
//  
//  /* Request device 0 to make a soft stop */
//  BSP_MotorControl_SoftStop(0);
//  
//  /* Wait for device 0 ends moving */  
//  BSP_MotorControl_WaitWhileActive(0);

//  /* Request device 1 to make a hard stop */
//  BSP_MotorControl_HardStop(1);

// /* Wait for device 1 ends moving */  
//  BSP_MotorControl_WaitWhileActive(1);

//  /* Wait for 2 seconds */
//  HAL_Delay(2000);  

//  /* Request both devices to go home */
//  BSP_MotorControl_GoHome(0);  
//  BSP_MotorControl_GoHome(1);  

//  /* Wait for both devices end moving */  
//  BSP_MotorControl_WaitWhileActive(0);
//  BSP_MotorControl_WaitWhileActive(1);

//  /* Wait for 2 seconds */
//  HAL_Delay(2000);  

//  /* Get acceleration, deceleration and MinSpeed of device 0*/
//  myAcceleration = BSP_MotorControl_GetAcceleration(0);
//  myDeceleration = BSP_MotorControl_GetDeceleration(0);
//  myMinSpeed = BSP_MotorControl_GetMinSpeed(0);
 
  /* Select ful step mode for device 1 */
	BSP_MotorControl_SelectStepMode(0, STEP_MODE_1_32);
  BSP_MotorControl_SelectStepMode(1, STEP_MODE_1_32);

  /* Set speed and acceleration of device 1 to be consistent with full step mode */
  BSP_MotorControl_SetMaxSpeed(0, myMaxSpeed / 16);
  BSP_MotorControl_SetMinSpeed(0, myMinSpeed / 16);
  BSP_MotorControl_SetAcceleration(0, myAcceleration / 16);
  BSP_MotorControl_SetDeceleration(0, myDeceleration / 16);
	
  BSP_MotorControl_SetMaxSpeed(1, myMaxSpeed / 16);
  BSP_MotorControl_SetMinSpeed(1, myMinSpeed / 16);
  BSP_MotorControl_SetAcceleration(1, myAcceleration / 16);
  BSP_MotorControl_SetDeceleration(1, myDeceleration / 16);
  
//  /* Request device 0 to go to position 400 */
//  BSP_MotorControl_GoTo(0, 6400);  

//  /* Request device 1 to go to the equivalent reverse position */
//  /* in full step mode -6400 /16 = -400 */
//  BSP_MotorControl_GoTo(1, -400 );  
//  
//  /* Wait for both devices end moving */  
//  BSP_MotorControl_WaitWhileActive(0);
//  BSP_MotorControl_WaitWhileActive(1);

//  /* Wait for 2 seconds */
//  HAL_Delay(2000);

//  /* Set the new position of both devices to be their new mark position */  
//  BSP_MotorControl_SetMark(0);
//  BSP_MotorControl_SetMark(1);
//  
//  /* Wait for 5 seconds before entering the loop */
//  HAL_Delay(5000);
}

#ifdef  USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

  /* Infinite loop */
  while (1)
  {
  }
}
#endif

/**
  * @}
  */


/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
