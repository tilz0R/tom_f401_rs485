#include "packet.h"

packet_t packet;

void PACKET_Init(void) {
    memset(&packet, 0x00, sizeof(packet));
}
    
packet_t* PACKET_Read(void) {
    uint8_t b;
    uint8_t ret = 0;
    while (USART_ReadByte(&b)) {
        switch (packet.state) {
            /* First state is packet start byte */
            case 0:
                if (b == PACKET_START_BYTE) {
                    packet.state++;
                }
                break;
            /* Check packet address */
            case 1:
                packet.address = b;
                packet.state++;
                break;
            /* Number of data bytes in packet */
            case 2:
                packet.length = b;
                packet.state++;
                break;
            /* Actual data */
            case 3:
                packet.data[packet.counter++] = b;
                packet.data[packet.counter] = 0;
                if (packet.length == packet.counter) {
                    packet.state++;
                }
                break;
            /* Check stop byte */
            case 4:
                if (b == PACKET_STOP_BYTE) {
                    packet.status = PACKET_STATUS_OK;
                } else {
                    packet.status = PACKET_STATUS_ERROR;
                    packet.length = 0;
                }
                ret = 1;
                packet.state = 0;
                packet.counter = 0;
                break;
            default:
                break;
        }
        if (ret) {
            break;
        }
    }
    
    if (ret) {
        return &packet;
    }
    return 0;
}

void PACKET_Write(packet_t* packet) {
    uint8_t i = 0;
    USART_WriteByte(PACKET_START_BYTE);
    USART_WriteByte(packet->address);
    USART_WriteByte(packet->length);
    for (i = 0; i < packet->length; i++) {
        USART_WriteByte(packet->data[i]);
    }
    USART_WriteByte(PACKET_STOP_BYTE);
}

