/* Includes */
#include "common.h"

/**
 * \brief           Get current device status if device is in bootloader or in main program
 * \param[in]       *pc: Pointer to \ref pc_t structure with packet data
 * \return          Non-zero on success, 0 on failure
 */
static uint8_t
pc_status(const pc_t* pc) {
    uint32_t val;
#if defined(BOOTLOADER)
    val = STATUS_BOOTLOADER;                /* Bootloader */
#else
    val = STATUS_IDLE;                      /* Main program */
#endif
    pc_set_resp_data(&val, sizeof(val));    /* Set response message */
    return 1;
}

/**
 * \brief           Get device details info such as firmware version, hardware version
 * \param[in]       *pc: Pointer to \ref pc_t structure with packet data
 * \return          Non-zero on success, 0 on failure
 */
static uint8_t
pc_details(const pc_t* pc) {
    uint32_t detail_data[5];
    
    /* Fill device description */
    detail_data[0] = flash_settings.head.sw_major;  /* Set device major version */
    detail_data[1] = flash_settings.head.sw_minor;  /* Set device minor version */
    detail_data[2] = flash_settings.head.hardware;  /* Set device hardware version */
    detail_data[3] = pc_getaddr_callback(NULL); /* Set device address */
    detail_data[4] = flash_settings.head.valid_program; /* Set main program valid status */
    pc_set_resp_data(detail_data, sizeof(detail_data)); /* Copy data to response buffer */
    
    return 1;
}

/**
 * \brief           Initializes bootloader and prepares commands for communication
 */
uint8_t
common_init(void) {
    flash_settings_init();                  /* Init flash settings */
    rtc_init();                             /* Initialize RTC for backup registers */
    //iwdg_init();                            /* Initialize independent watchdog */
    crc_init();                             /* Init hardware CRC */
    pc_init();                              /* Init packet control and corresponding hardware for it */    

    /** Assign commands for bootloader */
    pc_cmd_add(PC_CMD_STATUS, pc_status);
    pc_cmd_add(PC_CMD_DETAILS, pc_details);
    
    return 0;
}

/**
 * \brief           Callback from packet control to receive address for current device
 * \param[in]       *pc: Pointer to \ref pc_t structure with packet data. This parameter can be a NULL in some cases
 * \retval          Device address
 */
uint8_t
pc_getaddr_callback(const pc_t* pc) {
    return 0x80;
}

