/* Includes ------------------------------------------------------------------*/
#include "bootloader.h"
#include "rtc.h"

/**
 * \brief           Posible reset types
 */
typedef struct bl_reset_types_t {           
    uint8_t wdg_reset;                          /*!< Watchdog caused reset */
    uint8_t sw_reset;                           /*!< Reset was by software */
    uint8_t hard_reset;                         /*!< Flag indicating reset was hardware type */
} bl_reset_types_t;

/* Management for bootloader to stay in bootloader */
static uint8_t jump_to_main_flag;
static uint8_t stay_in_boot;

/* Flash update procedure */
static flash_t flash_update;
static uint32_t flash_update_counter;
static uint32_t flash_update_active;

/**
 * \brief           Read reset flags
 * \param[in]       *rst_type: Pointer to \ref bl_reset_types_t structure to save data to
 * \return          0 on success, non-zero on failure
 */
static uint8_t 
bl_get_reset_type(bl_reset_types_t* rst_types) {
    rst_types->wdg_reset = __HAL_RCC_GET_FLAG(RCC_FLAG_IWDGRST);
    rst_types->hard_reset = __HAL_RCC_GET_FLAG(RCC_FLAG_PORRST) | __HAL_RCC_GET_FLAG(RCC_FLAG_PINRST);
    rst_types->sw_reset = __HAL_RCC_GET_FLAG(RCC_FLAG_SFTRST);
    
    __HAL_RCC_CLEAR_RESET_FLAGS();              /* Clear all reset flags */
    
    return 0;
}

/**
 * \brief           jumps to main
 * \return          0 on success, non-zero on failure
 */
static uint8_t
bl_jump_to_main() {
    void (*user_code_entry)(void);
	int dummy_delay = 1000;
	while(dummy_delay--);
	
	RCC->CIR = 0x009F0000; //Disable all interrupts and clear pending bits
    
    //TODO: move this to constant
	SCB->VTOR = (APPLICATION_ADDR & 0x1FFFFF80);
	user_code_entry =  (void (*)(void)) (*(uint32_t *)(APPLICATION_ADDR + 4));
	
	// Jump to user application
	user_code_entry();
    
    return 0;
}

/**
 * \brief           Jump to main command received over RS485
 * \param[in]       *pc: Pointer to \ref pc_t structure with packet data
 * \return          Non-zero on success, 0 on failure
 */
static uint8_t
bl_cmd_jump_to_main(const pc_t* pc) {
    /* Set the flag for bootloader jump. We must still respond to command */
    /* Jump is performed in bootloader loop function */
    jump_to_main_flag = 1;
    
    return 1;                                   /* Return 1 means success */
}

/**
 * \brief           Erase main program from flash
 * \param[in]       *pc: Pointer to \ref pc_t structure with packet data
 * \return          Non-zero on success, 0 on failure
 */
static uint8_t
bl_cmd_erase(const pc_t* pc) {
    uint32_t app_len;
    const uint32_t* data = (const uint32_t *)pc->data;
    
    app_len = *data++;                          /* Get length of app for erase */
    
    /* Try to init flash procedure */
    if (flash_init(&flash_update, APPLICATION_ADDR, app_len)) {
        return 0;                               /* Return error */
    }
    if (flash_erase(&flash_update)) {           /* Try to erase all required sectors for main application */
        return 0;                               /* Return error */
    }
    flash_update_counter = 0;                   /* Reset update counter */
    flash_update_active = 1;                    /* Activate flash update */
    
    flash_settings.head.valid_program = 0;      /* Program is not valid anymore */
    flash_settings_update();                    /* Update flash settings */
    
    return 1;                                   /* Response with OK */
}

/**
 * \brief           Start for firmware update
 * \note            After this command, data for update will be received
 * \param[in]       *pc: Pointer to \ref pc_t structure with packet data
 * \return          Non-zero on success, 0 on failure
 */
static uint8_t
bl_cmd_fw_start(const pc_t* pc) {
    if (!flash_update_active) {                 /* We are not in flash update process */
        return 0;
    }
    return 1;
}

/**
 * \brief           Packet with firmware data
 * \param[in]       *pc: Pointer to \ref pc_t structure with packet data
 * \return          Non-zero on success, 0 on failure
 */
static uint8_t
bl_cmd_fw_data(const pc_t* pc) {
    uint32_t pcnt;
    const uint32_t* data = (const uint32_t *)pc->data;
    
    if (!flash_update_active) {                 /* We are not in flash update process */
        return 0;
    }
    
    /**
     * Check packet counter versus last written packet number
     *
     * - If new packet is less than last written packet = ERROR
     * - If new packet is greater than last written packet + 1 = ERROR
     * - If new packet is one more than last written packet = OK + write this packet to flash
     * - If new packet is the same as last written packet = OK without writing to flash
     */
    pcnt = *data++;                             /* Get length of app for erase */
    pc_set_resp_data(&pcnt, 4);                 /* Set counter as response */
    if (pcnt < flash_update_counter || pcnt > (flash_update_counter + 1)) {
        return 0;                               /* Serious error */
    } else if (pcnt == flash_update_counter) {  /* Check if counters the same */
        return 1;                               /* Return OK but don't write anything to flash */
    }
    
    /* Try to init flash procedure */
    if (flash_program(&flash_update, data, pc->len - 4)) {
        return 0;                               /* Return error message */
    }
    flash_update_counter = pcnt;                /* Save last packet number for flash update */
    return 1;                                   /* Response with OK */
}

/**
 * \brief           Firmware end info received, check if CRC of firmware is OK and ready to be 
 * \param[in]       *pc: Pointer to \ref pc_t structure with packet data
 * \return          Non-zero on success, 0 on failure
 */
static uint8_t
bl_cmd_fw_end(const pc_t* pc) {
    const uint32_t* data = (const uint32_t *)pc->data;
    
    if (!flash_update_active) {                 /* We are not in flash update process */
        return 0;
    }
    
    if (flash_finish(&flash_update)) {          /* Finish flash procedure */
        return 0;
    }
    
    /* CRC check which is at the end of program, must return zero */
    if (crc_compute((void *)flash_update.start_addr, flash_update.total_len >> 2, 1)) {
        return 0;                               /* Serious error */
    }
    
    /* Set new values for program version */
    flash_settings.head.sw_major = *data++;     /* First word is major software version */
    flash_settings.head.sw_minor = *data++;     /* Second word is minor software version */
    flash_settings.head.valid_program = APPLICATION_VALID;  /* Program is valid */
    
    if (flash_settings_update()) {              /* Write settings about new version */
        return 0;
    }
    flash_update_active = 0;                    /* Clear active flag for update */
    return 1;
}

/**
 * \brief           Initializes bootloader and prepares commands for communication
 * \return          0 on success, non-zero on failure
 */
uint8_t
bootloader_init(void) {
    bl_reset_types_t rst_types;
    uint8_t read_boot;
    
	bl_get_reset_type(&rst_types);              /* Get the reset type */
    read_boot = rtc_read_boot();                /* get backup registers */
    if (
        !read_boot ||                           /* Forced from main program */
        flash_settings.head.valid_program != APPLICATION_VALID ||   /* Invalid program while programming */
        ((*(uint32_t *)APPLICATION_ADDR) & 0xF0000000UL) != 0x20000000  /* Actual program check is not valid */
    ) {                                         /* Stay in bootloader */
        rtc_clear_boot();                       /* Reset backup for next reset */
        stay_in_boot = 1;                       /* Set flag for staying in bootloader */
    }
    
    /* Assign commands for bootloader */
    pc_cmd_add(PC_CMD_JUMP_TO_MAIN, bl_cmd_jump_to_main);
    pc_cmd_add(PC_CMD_ERASE, bl_cmd_erase);
    pc_cmd_add(PC_CMD_FW_START, bl_cmd_fw_start);
    pc_cmd_add(PC_CMD_FW_DATA, bl_cmd_fw_data);
    pc_cmd_add(PC_CMD_FW_END, bl_cmd_fw_end);
    
    return 0;
}

/**
 * \brief           Bootloader loop software for periodic run
 * \note            This code should not be modified
 * \return          0 on success, non-zero on failure
 */
uint8_t 
bootloader_loop(void) {
    //TODO: use other criteria for staying in boot
    if (!stay_in_boot) {
        bl_jump_to_main();                      /* Perform jump to main application */
    }
    if (jump_to_main_flag) {
        bl_jump_to_main();
    }

    return 0;
}
