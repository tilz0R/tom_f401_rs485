/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __CRC_H
#define __CRC_H
#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"
#include "main.h"
     
extern CRC_HandleTypeDef hcrc;

uint8_t crc_init(void);
uint32_t crc_compute(void* addr, uint32_t len, uint32_t reset);
     
#ifdef __cplusplus
}
#endif
#endif /*__ __CRC_H */
