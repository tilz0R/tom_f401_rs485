/**
  ******************************************************************************
  * File Name          : USART.c
  * Description        : This file provides code for the configuration
  *                      of the USART instances.
  ******************************************************************************
  ** This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * COPYRIGHT(c) 2017 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "usart.h"
#include "gpio.h"

#define RS485_USART2

#if defined(RS485_USART1)
/* USART setup */
#define COM_USART                           USART1
#define COM_USART_CLK_ENABLE()              __HAL_RCC_USART1_CLK_ENABLE()
#define COM_USART_IRQn                      USART1_IRQn
#define COM_USART_IRQHandler                USART1_IRQHandler

/* GPIO setup */
#define COM_USART_TX_PORT                   GPIOA
#define COM_USART_TX_PIN                    GPIO_PIN_9
#define COM_USART_RX_PORT                   GPIOA
#define COM_USART_RX_PIN                    GPIO_PIN_10
#define COM_USART_AF                        GPIO_AF7_USART1
#define COM_USART_TX_PIN_CLK_ENABLE()       __HAL_RCC_GPIOA_CLK_ENABLE()
#define COM_USART_RX_PIN_CLK_ENABLE()       __HAL_RCC_GPIOA_CLK_ENABLE()
#elif defined(RS485_USART2)
/* USART setup */
#define COM_USART                           USART2
#define COM_USART_CLK_ENABLE()              __HAL_RCC_USART2_CLK_ENABLE()
#define COM_USART_IRQn                      USART2_IRQn
#define COM_USART_IRQHandler                USART2_IRQHandler

/* GPIO setup */
#define COM_USART_TX_PORT                   GPIOA
#define COM_USART_TX_PIN                    GPIO_PIN_2
#define COM_USART_RX_PORT                   GPIOA
#define COM_USART_RX_PIN                    GPIO_PIN_3
#define COM_USART_AF                        GPIO_AF7_USART2
#define COM_USART_TX_PIN_CLK_ENABLE()       __HAL_RCC_GPIOA_CLK_ENABLE()
#define COM_USART_RX_PIN_CLK_ENABLE()       __HAL_RCC_GPIOA_CLK_ENABLE()
#endif

/* RS-485 data enable pin */
#define COM_USART_DE_PORT                   GPIOC
#define COM_USART_DE_PIN                    GPIO_PIN_6
#define COM_USART_DE_PIN_CLK_ENABLE()       __HAL_RCC_GPIOC_CLK_ENABLE()

/* Buffer variable */
static
BUFFER_t* receive_buffer;

uint8_t
usart_init(BUFFER_t* rx_buff) {
    GPIO_InitTypeDef GPIO_InitStruct;
    LL_USART_InitTypeDef USART_InitStruct;
    
    /* Save RX buffer */
    receive_buffer = rx_buff;
    
    /** Configure GPIO pins for UART */
    COM_USART_TX_PIN_CLK_ENABLE();
    COM_USART_RX_PIN_CLK_ENABLE();
    COM_USART_DE_PIN_CLK_ENABLE();
    
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_PULLUP;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    GPIO_InitStruct.Alternate = COM_USART_AF;
    GPIO_InitStruct.Pin = COM_USART_TX_PIN;
    HAL_GPIO_Init(COM_USART_TX_PORT, &GPIO_InitStruct);
    GPIO_InitStruct.Pin = COM_USART_RX_PIN;
    HAL_GPIO_Init(COM_USART_RX_PORT, &GPIO_InitStruct);
    
    /* Init data enable pin */
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_PULLDOWN;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
    GPIO_InitStruct.Pin = COM_USART_DE_PIN;
    HAL_GPIO_Init(COM_USART_DE_PORT, &GPIO_InitStruct);
    LL_GPIO_ResetOutputPin(COM_USART_DE_PORT, COM_USART_DE_PIN);
    
    /* Configure UART */
    COM_USART_CLK_ENABLE();
    
    LL_USART_StructInit(&USART_InitStruct);
    USART_InitStruct.BaudRate = 115200;
    USART_InitStruct.DataWidth = LL_USART_DATAWIDTH_8B;
    USART_InitStruct.HardwareFlowControl = LL_USART_HWCONTROL_NONE;
    USART_InitStruct.OverSampling = LL_USART_OVERSAMPLING_16;
    USART_InitStruct.Parity = LL_USART_PARITY_NONE;
    USART_InitStruct.StopBits = LL_USART_STOPBITS_1;
    USART_InitStruct.TransferDirection = LL_USART_DIRECTION_TX_RX;
    LL_USART_Init(COM_USART, &USART_InitStruct);
    LL_USART_EnableIT_RXNE(COM_USART);
    LL_USART_Enable(COM_USART);
    
    /* Enable interrupts */
    HAL_NVIC_SetPriority(COM_USART_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(COM_USART_IRQn);
    
    return 0;
}

/**
 * \brief           Send data over RS485 protocol
 * \param[in]       *data: Pointer to data to send
 * \param[in]       len: Number of bytes to send
 * \return          0 on success, non-zero on failure
 */
uint8_t
usart_send(const void* data, size_t len) {
    volatile uint32_t timeout;
    const uint8_t* ptr = data;
    
    if (!len || !data) {
        return 1;
    }
    
    LL_GPIO_SetOutputPin(COM_USART_DE_PORT, COM_USART_DE_PIN);
    timeout = 0x4FFF; while (timeout--);
    while (len--) {
        LL_USART_TransmitData8(COM_USART, *ptr++);  /* Send data byte */
        while (!LL_USART_IsActiveFlag_TXE(COM_USART));  /* Wait till transmission completed */
    }
    timeout = 0x4FFF; while (timeout--);
    LL_GPIO_ResetOutputPin(COM_USART_DE_PORT, COM_USART_DE_PIN);
    
    return 0;
}

/**
 * \brief           Interrupt handler for communication UART
 */
void
COM_USART_IRQHandler(void) {
    /* Check RX not empty flag */
    if (LL_USART_IsActiveFlag_RXNE(COM_USART)) {
        uint8_t d;
        LL_USART_ClearFlag_RXNE(COM_USART);
        
        d = COM_USART->DR;                  /* Read data from UART */
        BUFFER_Write(receive_buffer, &d, 1);/* Write data to buffer */
    }
}


/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
