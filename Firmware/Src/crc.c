/* Includes ------------------------------------------------------------------*/
#include "crc.h"

/* CRC handle structure from HAL */
CRC_HandleTypeDef hcrc;

/**
 * \brief       Initialize CRC peripheral
 * \retval      0: Initialized OK
 * \retval      1: Initialization error
 */
uint8_t
crc_init(void) {
    __HAL_RCC_CRC_CLK_ENABLE();             /* Enable CRC clock */
    
    hcrc.Instance = CRC;                    /* Set CRC peripheral */
    
    /* Try to init CRC */
    return HAL_CRC_Init(&hcrc) == HAL_OK ? 0 : 1;
}

uint32_t
crc_compute(void* addr, uint32_t len, uint32_t reset) {
    return HAL_CRC_Calculate(&hcrc, addr, len);
}
