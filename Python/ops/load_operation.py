from comm import rs485_commands
from sched.arguments import AmountArgument, ReagentArgument
from sched.operations import Operation
import time

class LoadOperation(Operation):
    def __init__(self, serial_controller):
        self._controller = serial_controller

        Operation.__init__(self, "load")

    def execute(self, arguments, system):

        amount = self.get_argument("amount", arguments)

        time.sleep(5)

        # might raise NoResponseException or ErrorPacketException in which case the execution will pause
        response = self._controller.send_receive(system.get_address("A"), rs485_commands.PC_CMD_STATUS)

    def get_arguments(self):
        return [
            AmountArgument("amount"),
            ReagentArgument("reagent")
        ]