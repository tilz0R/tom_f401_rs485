#include "main.h"
#include "stm32f4xx_hal.h"
#include "common.h"

/* Private function prototypes */
void SystemClock_Config(void);

int main(void) {
    /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
    HAL_Init();
    SystemClock_Config();

    /* Initialize all configured peripherals required for bootloader and for main application */    
    common_init();                          /* Initialize common commands between bootloader and main program */
    
#if defined(BOOTLOADER)
    bootloader_init();                      /* Initialize bootloader */
#else
    app_init();                             /* Initialize main program */
    app_userinit();                         /* Initialize user specific part for device type */
#endif /* defined(BOOTLOADER) */
    
    while (1) {
        pc_process();                       /* Process packet control for incoming data */
        
#if defined(BOOTLOADER)
        bootloader_loop();                  /* Run bootloader loop */
#else
        /**
         * Main application super loop
         * Should not be modified by user
         */
        app_loop();                         /* Run main program loop */
        
        /**
         * User specific code for project
         * Each application should have different userloop function
         */
        app_userloop();                     /* Run loop for specific device type */
#endif /* defined(BOOTLOADER) */
        
        iwdg_reload();                      /* Reload IWDG counter */
    }
}

/** 
 * \brief           System Clock Configuration
 * \note            Configures system clock for future use
 */
void SystemClock_Config(void) {
    RCC_OscInitTypeDef RCC_OscInitStruct;
    RCC_ClkInitTypeDef RCC_ClkInitStruct;

    /* Configure the main internal regulator output voltage */
    __HAL_RCC_PWR_CLK_ENABLE();
    __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE2);

    /* Initializes the CPU, AHB and APB busses clocks */
    RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
    RCC_OscInitStruct.HSIState = RCC_HSI_ON;
    RCC_OscInitStruct.HSICalibrationValue = 16;
    RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
    RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
    RCC_OscInitStruct.PLL.PLLM = 16;
    RCC_OscInitStruct.PLL.PLLN = 336;
    RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV4;
    RCC_OscInitStruct.PLL.PLLQ = 4;
    if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK) {
        _Error_Handler(__FILE__, __LINE__);
    }

    /* Initializes the CPU, AHB and APB busses clocks */
    RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                                    | RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
    RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
    RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
    RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
    RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

    if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK) {
        _Error_Handler(__FILE__, __LINE__);
    }

    /* Configure the Systick interrupt time */
    HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);
    HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);
    HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/**
 * \brief       This function is executed in case of error occurrence.
 * \param       None
 * \retval      None
 */
void _Error_Handler(char * file, int line) {
    /* User can add his own implementation to report the HAL error return state */
    while(1) {

    }
}

#ifdef USE_FULL_ASSERT

/**
 * \brief       Reports the name of the source file and the source line number
 *                  where the assert_param error has occurred.
 * \param       file: pointer to the source file name
 * \param       line: assert_param error line source number
 * \retval      None
 */
void assert_failed(uint8_t* file, uint32_t line) {
    __NOP();
}

#endif

