import ConfigParser
import os
import flog

DEFAULT_LOGFILE = "log.txt"
DEFAULT_RETRIES = 1

def parse(configfile):
    config = ConfigParser.RawConfigParser()
    config.read(configfile)

    settings = {}

    # SERIAL
    # port
    try:
        port = config.get('Serial', 'port')
    except Exception as e:
        flog.flog_error("ConfigParse: Serial port not valid")
        raise e;
    settings["port"] = port
    
    # baud
    try:
        baud = config.getint('Serial', 'baud')
    except Exception as e:
        flog.flog_error("ConfigParse: Serial baud not valid")
        raise e;
    settings["baud"] = baud

    # rs485 strategy
    try:
        rs485_strategy = config.get('Serial', 'rs485_strategy')

        if rs485_strategy not in ['gpio', 'dtr', 'usb']:
            raise Exception;
    except Exception as e:
        flog.flog_error("ConfigParse: RS485 strategy not valid")
        raise e;
    settings["rs485_strategy"] = rs485_strategy

    # rs485 retries
    rs485_retries = DEFAULT_RETRIES
    try:
        rs485_retries = config.getint('Serial', 'rs485_retries')

    except Exception as e:
        pass
    settings["rs485_retries"] = rs485_retries


    # LOGGING
    # logfile
    dirname, filename = os.path.split(os.path.abspath(__file__))
    logfile = dirname + "/" + DEFAULT_LOGFILE;
    try:
        logfile = config.get('Logging', 'logfile')
    except Exception as e:
        pass;
    settings["logfile"] = logfile

    loglevel = flog.FLog.LEVEL_INFO;
    try:
        loglevel_config = config.get('Logging', 'loglevel')
        if loglevel_config == "debug" or loglevel_config == "4": loglevel = flog.FLog.LEVEL_DEBUG
        if loglevel_config == "info"  or loglevel_config == "3": loglevel = flog.FLog.LEVEL_INFO
        if loglevel_config == "warning"  or loglevel_config == "2": loglevel = flog.FLog.LEVEL_WARN
        if loglevel_config == "error" or loglevel_config == "1": loglevel = flog.FLog.LEVEL_ERRO
    except Exception as e:
        pass;
    settings["loglevel"] = loglevel

    return settings;