
def crc32_slow(crc,data):
    crc = crc ^ data;

    for i in range(32):
        if (crc & 0x80000000):
            crc = ((crc << 1) & 0xffffffff) ^ 0x04C11DB7; #Polynomial used in STM32
        else:
            crc = ((crc << 1) & 0xffffffff);

    return crc

def crc32_stm32(data):
    crc = crc32_slow(0xffffffff, (data[3]<<24) + (data[2]<<16) + (data[1]<<8) + data[0]) # big endian
    i=4

    while True:
        if len(data) >= (i+4):
            crc = crc32_slow(crc, (data[i+3]<<24) + (data[i+2]<<16) + (data[i+1]<<8) + data[i])
            i+=4
        if len(data) == i:
            return crc