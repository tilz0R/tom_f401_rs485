#!/usr/bin/env python
#

# import modules used here -- sys is a very standard one
import sys, argparse, logging
import struct

START_BYTE = 0xAA
STOP_BYTE = 0x55

def crc32_slow(crc,data):
    crc = crc ^ data;

    for i in range(32):
        if (crc & 0x80000000):
            crc = ((crc << 1) & 0xffffffff) ^ 0x04C11DB7; #Polynomial used in STM32
        else:
            crc = ((crc << 1) & 0xffffffff);

    return crc

def crc32_stm32(data):
    crc = crc32_slow(0xffffffff, (data[3]<<24) + (data[2]<<16) + (data[1]<<8) + data[0]) # big endian
    i=4

    while True:
        if len(data) >= (i+4):
            crc = crc32_slow(crc, (data[i+3]<<24) + (data[i+2]<<16) + (data[i+1]<<8) + data[i])
            i+=4
        if len(data) == i:
            return crc

def PacketEncode(address_from, address_to, cmd, data):
    comm_len = 0

    bytes_out = chr(START_BYTE)

    if data != None:
        comm_len = len(data);
    
    crc_data = struct.pack('BBBB', int(address_from), int(address_to), int(cmd), comm_len);

    if data != None:
        crc_data += data;

    #crc_value = crc16_func(crc_data)
    crc_value = crc32_stm32(crc_data)

    bytes_out += crc_data + struct.pack('<H', crc_value);

    bytes_out += chr(STOP_BYTE);

    return bytes_out

# Gather our code in a main() function
def main(args, loglevel):
  logging.basicConfig(format="%(levelname)s: %(message)s", level=loglevel)
  
  # TODO Replace this with your actual code.
  print ("Hello there.")
  logging.info("You passed an argument.")
  logging.debug("Your Argument: %s" % sys.argv[2])
  logging.debug("Your Argument: %s" % sys.argv[3])
  logging.debug("Your Argument: %s" % sys.argv[4])
  logging.debug("Your Argument: %s" % sys.argv[5])
  address_from = 0x01
  address_to = 0x80
  cmd = 0x03
  data = struct.pack('BBBB', int(sys.argv[2]),int(sys.argv[3]),int(sys.argv[4]),int(sys.argv[5]));
  bytesout = PacketEncode(address_from, address_to, cmd, data)
  #crc = crc32_stm32(crc_data)
  #print(crc)

# Standard boilerplate to call the main() function to begin
# the program.
if __name__ == '__main__':
  parser = argparse.ArgumentParser()
  # TODO Specify your real parameters here.
  parser.add_argument("data3", type=int)
  parser.add_argument("data2", type=int)
  parser.add_argument("data1", type=int)
  parser.add_argument("data0", type=int)
  parser.add_argument("-v", "--verbose", help="increase output verbosity", action="store_true")
  args = parser.parse_args()
  
  # Setup logging
  if args.verbose:
    loglevel = logging.DEBUG
  else:
    loglevel = logging.INFO
  
main(args, loglevel)