Information regarding file structure.

Project is structured in a way to support BOOTLOADER and all future main programs in the same package.
They are separated in different targets, where one is "Bootloader" and all others are "Main programs" for different types of boards.

For every future device, just create new target by copying one of main application targets.
When you create new target, make sure your current target is not bootloader as this will copy bootloader target and code won't work.

Memory structure on board:
0x0800 0000 - 0x0800 3FFF: BOOTLOADER
0x0800 4000 - 0x0800 7FFF: FLASH SETTINGS
0x0800 8000 - end of flash: MAIN APPLICATION

Since bootloader and main apps are in the same project,
they can easily reuse parts of code which makes it easy to follow (change of GPIOs, etc) without having
duplicated code and allows you to easily track bugs in comparison to duplicated code.

3 parts of code can be found:
- COMMON: Common code for bootloader and main app. You can find here rtc_init, iwdg, packet control, and commands for status and details
- BOOTLOADER: Bootloader specific code: Jump to main, program device and commands for erase, program, etc flash.
- APP: User specific code for special device.

When user creates new device, it must create 2 important functions:

- app_userinit: Called on start to initialize peripherals required for specific app, can be used to assign specific commands for packets for specific device (for example, read motor speed, set motor direction, etc)

- app_userloop: Called inside while (1) to constantly process user data

