/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __APPLICATION_H
#define __APPLICATION_H

#ifdef __cplusplus
 extern "C" {
#endif

#include "stdint.h"
#include "packet.h"

/* Main application specific setup and should not be touched by user */
uint8_t app_init(void);
uint8_t app_loop(void);
     
/**
 * User and application specific settings
 *
 * Every device type uses these 2 functions for init in main() function and for user main loop
 *
 * Each device type (project type) uses different file but all of them include this file "application.h"
 *
 * User must implement these 2 functions, even if they are empty and are not used
 *
 * \note        Believe me, you will need them ;)
 */
uint8_t app_userinit(void);
uint8_t app_userloop(void);

#ifdef __cplusplus
}
#endif

#endif /* __APPLICATION_H */
