/* Includes ------------------------------------------------------------------*/
#include "packet.h"
#include "crc16.h"

/* Receive UART buffer processing */
static
uint8_t packet_data_buff[PC_RX_BUFFER_SIZE + 1];    /* Buffer for RX data bytes */
static
BUFFER_t packet_buff;               /* Packet buffer structure */

/* Raw data for packet to be sent back */
static
uint8_t packet_resp_data[sizeof(pc_t)]; /* Raw data to send response including headers and footers */
static uint8_t
packet_resp_data_len;               /* Number of actual data bytes to be send back (only data part, excluding header and footer) */

/* Packet structure with parsed data */
static
pc_t packet;                        /* Packet structure */

/* List of callbacks for commands */
static
pc_cmd_t* cmd_list_start;           /* Pointer to first command callback in linked list */


/**
 * \brief           Calculates CRC from packet
 * \param[in]       pc: Packet control structure
 * \retval          Calculated packet CRC
 */
static uint16_t
pc_calculate_crc(pc_t* pc) {
    size_t i;
    uint16_t result;
    
    result = CalcCrc16pt((char)pc->from, Init); /* Start calculating CRC with from address */
    result = CalcCrc16pt((char)pc->to, Add);    /* Add to address */
    result = CalcCrc16pt((char)pc->cmd, Add);   /* Add command number */
    result = CalcCrc16pt((char)pc->len, Add);   /* Add length number */
    
    /* Process data bytes */
    for (i = 0; i < pc->len; i++) { 
        result = CalcCrc16pt((char)pc->data[i], Add); /* Start calculating CRC */
    }
    
    return result;
}

/**
 * \brief           Send packet back to master with specific command
 * \note            If resp == 0, then PC_CMD_ERROR will be set as response command
 * \param[in]       cmd: Command value from packet
 * \return          0 on success, non-zero on failure
 */
static uint8_t
pc_send_response(pc_t* pc, uint8_t resp) {
    uint8_t cmd = resp ? pc->cmd : PC_CMD_ERROR;
    uint8_t* ptr = packet_resp_data;
    uint16_t crc;
    
    *ptr++ = PC_START_BYTE;
    *ptr++ = pc_getaddr_callback(NULL);
    *ptr++ = pc->from;
    *ptr++ = cmd;
    *ptr++ = packet_resp_data_len;
    ptr += packet_resp_data_len;
    
    /* Calculate CRC on all data + 4 header bytes, excluding start byte */
    crc = CalcCrc16((char *)(packet_resp_data + 1), packet_resp_data_len + 4);
    *ptr++ = (uint8_t)crc;
    *ptr++ = (uint8_t)(crc >> 8);
    *ptr++ = PC_STOP_BYTE;
    
    /* Send here the data */
    /* Send actual packet size + 1x START + 4x HEADER + data + 2xCRC + 1x STOP */
    HAL_Delay(20);
    usart_send(packet_resp_data, packet_resp_data_len + 8);
    
    return 0;
}

static void
pc_reset_resp_data(void) {
    packet_resp_data_len = 0;
}

/**
 * \brief           Initialize packet control module
 * \return          0 on success, non-zero on failure
 */
uint8_t
pc_init(void) {
    BUFFER_Init(&packet_buff, PC_RX_BUFFER_SIZE, packet_data_buff);
    usart_init(&packet_buff);               /* Init USART with packet buffer */
    
    return 0;                               /* Return OK */
}

/**
 * \brief           Process input packet data
 * \note            Must be called periodically in main loop
 * \return          Number of packets processed in the loop
 */
uint8_t 
pc_process(void) {
    uint8_t retval = 0;
    uint8_t d;
    pc_t* pc = &packet;
    
    while (BUFFER_Read(&packet_buff, &d, 1)) {  /* Read data from memory */
        switch (pc->sc) {                   /* Process state machine */
            case 0:
                if (d == PC_START_BYTE) {
                    memset(pc, 0x00, sizeof(*pc));  /* Reset structure */
                    pc->sc++;
                }
                pc->state = PC_STATE_WAITING;
                break;
            case 1:
                pc->from = d;
                pc->sc++;
                break;
            case 2:
                pc->to = d;
                pc->sc++;
                break;
            case 3:
                pc->cmd = d;
                pc->sc++;
                break;
            case 4:
                pc->len = d;
                if (!pc->len) {             /* When packet has no length... */
                    pc->sc++;               /* ...ignore data processing */
                }
                pc->sc++;
                break;
            case 5:
                pc->data[pc->mc++] = d;
                if (pc->mc >= pc->len) {
                    pc->sc++;
                }
                break;
            case 6:
                pc->crc = (pc->crc >> 8) | ((uint32_t)((uint32_t)d << 8));
                pc->cc++;
                if (pc->cc >= 2) {
                    pc->sc++;
                }
                break;
            case 7:
                pc->state = d == PC_STOP_BYTE ? PC_STATE_VALID : PC_STATE_ERROR;
                pc->sc = 0;
                break;
            default:
                pc->sc = 0;
        };
        
        /**
         * Now process packet
         */
        switch (pc->state) {
            case PC_STATE_VALID: {          /* We have valid packet */
                if (pc_calculate_crc(pc) == pc->crc) {  /* Check if CRC matches */
                    if (pc->from == PC_ADDR_MASTER) {   /* Check if it was sent from master */
                        uint8_t is_broadcast = pc->to == PC_ADDR_BROADCAST;
                        uint8_t is_addrmatch = pc->to == pc_getaddr_callback(pc);
                        /* Check if packet is for up */
                        if (is_broadcast || is_addrmatch) { /* This packet is for us */
                            pc_cmd_t* cmd = NULL;
                            /* Find if we have command with value */
                            for (cmd = cmd_list_start; cmd; cmd = cmd->next) {
                                if (cmd->cmd == pc->cmd) {  /* Compare received command with list of commands */
                                    break;  /* When found, just break and continue later */
                                }
                            }
                            uint8_t resp = 0;
                            pc_reset_resp_data();   /* Reset data to be sent back */
                            if (cmd) {      /* Valid command found */
                                resp = cmd->cb(pc); /* Process callback */
                            } else {        /* No command callback with received cmd */
                                pc_error_callback(pc, PC_ERROR_INVALID_CMD);
                            }
                            if (!is_broadcast) {    /* Do not respond on broadcast */
                                pc_send_response(pc, resp); /* Send response */
                            }
                        }
                    }
                } else {
                    pc_error_callback(pc, PC_ERROR_CRC);
                }
                break;
            }
            case PC_STATE_ERROR: {
                break;
            }
            default:
                break;
        }
        pc->lrt = HAL_GetTick();            /* Save time when byte was last received */
    }
    
    /**
     * Check timeout when bytes was received last time if there was more than timeout value
     * In this case, reset packet counter and mark packet as invalid
     */
    if (HAL_GetTick() - pc->lrt > 100 && pc->lrt) {
        pc->lrt = 0;                        /* Reset last received time */
        pc->sc = 0;                         /* Reset packet state machine */
    }
    
    return retval;
}

/**
 * \brief           Register new command with callback function
 * \param[in]       cmd: Command value from packet
 * \param[in]       cb: Callback used when packet with command is received
 * \return          0 on success, non-zero on failure
 */
uint8_t pc_cmd_add(uint8_t cmd, pc_cmd_cb_t cb) {
    pc_cmd_t* last_cmd = NULL;
    pc_cmd_t* new_cmd;
    
    if (!cb) {                              /* Check if callback is set */
        return 1;
    }
    
    /* Check if there is already registered command */
    for (new_cmd = cmd_list_start; new_cmd; new_cmd = new_cmd->next) {
        if (new_cmd->cmd == cmd) {
            return 1;
        }
    }
    
    new_cmd = malloc(sizeof(*new_cmd));     /* Allocate memory for new command */
    if (new_cmd) {                          /* Memory allocated */
        memset(new_cmd, 0x00, sizeof(*new_cmd));
        
        new_cmd->cmd = cmd;                 /* Set command value */
        new_cmd->cb = cb;                   /* Set callback function */
        
        if (cmd_list_start) {               /* First command is already set */
            /* Get last command in list */
            for (last_cmd = cmd_list_start; last_cmd->next; last_cmd = last_cmd->next) {}
            last_cmd->next = new_cmd;       /* Add command to the last in linked list */
        } else {
            cmd_list_start = new_cmd;       /* This is first command */
        }
        return 0;                           /* Function succedded */
    }
    return 1;
}

/**
 * \brief           Assign data to be sent back on packet
 * \note            Should be called only from callback function
 * \param[in]       *data: Pointer to data to be sent back
 * \param[in]       len: Size of data in units of bytes
 * \return          0 on success, non-zero on failure
 */
uint8_t
pc_set_resp_data(const void* data, uint8_t len) {
    if (!data || len > PC_MAX_PACKET_SIZE) {
        return 1;
    }
    packet_resp_data_len = len;             /* Set length of data we will copy */
    memcpy(&packet_resp_data[5], data, len);/* Copy data directly to sent buffer */
    return 0;
}

/**
 * \brief           Callback function in special events
 * \param[in]       *pc: Pointer to \ef pc_t packet where error occurred
 * \param[in]       err: Error type. This parameter can be a value of \ref pc_error_t enumeration
 */
__weak void 
pc_error_callback(const pc_t* pc, pc_error_t err) {
    
}

/**
 * \brief           Callback to get address for device
 * \param[in]       *pc: Pointer to \ref pc_t structure. Can also be NULL when retrieve for address is used to send packet
 * \retval          Device address
 */
__weak uint8_t
pc_getaddr_callback(const pc_t* pc) {
    return 0;
}

