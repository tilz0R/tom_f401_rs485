#include "usart.h"

//DMA TX buffer
TM_BUFFER_t TX_BUFFER;
uint8_t TX_BUFFER_Data[512];
DMA_Stream_TypeDef* TX_Stream;
#define TXDMASIZE                20

static uint8_t txDMAsize = 0;

void start_sending(void) {
    txDMAsize = TXDMASIZE;
	if (TX_BUFFER.Out <= TX_BUFFER.In) {
		if (TX_BUFFER.In - TX_BUFFER.Out <= TXDMASIZE){
			txDMAsize = TX_BUFFER.In - TX_BUFFER.Out;
        }
	} else {
		if (TX_BUFFER.Size - TX_BUFFER.Out <= TXDMASIZE) {
			txDMAsize = TX_BUFFER.Size - TX_BUFFER.Out;
		}
	}
    TM_USART_DMA_Send(USARTx, &TX_BUFFER.Buffer[TX_BUFFER.Out], txDMAsize);
}

/* Check DMA if we should send something */
__STATIC_INLINE void check_TX_DMA(void) {
    if (TX_Stream->NDTR) {
        return;
    }
    if (TM_BUFFER_GetFull(&TX_BUFFER)) {
        start_sending();
    }
}
/*************************/
/*   Public functions    */
/*************************/
void USART_Init(void) {
    /* Init TX buffer for DMA */
    TM_BUFFER_Init(&TX_BUFFER, sizeof(TX_BUFFER_Data) - 1, TX_BUFFER_Data);
    
    /* Init UART and UART DMA */
    TM_USART_Init(USARTx, USARTx_PP, USARTx_BAUDRATE);
		//MX_USART6_UART_Init();
    TM_USART_DMA_Init(USARTx);
    
    /* Enable DMA interrupts for USART */
    TM_USART_DMA_EnableInterrupts(USARTx);
    
    /* Get TX stream and save it */
    TX_Stream = TM_USART_DMA_GetStreamTX(USARTx);
}

//Write single byte to uart tx buffer
void USART_WriteByte(const uint8_t data) {
    TM_BUFFER_Write(&TX_BUFFER, &data, 1);
    check_TX_DMA();
}

//Write multiple bytes to uart tx buffer
void USART_WriteData(const uint8_t* data, size_t length) {
    TM_BUFFER_Write(&TX_BUFFER, data, length);
    check_TX_DMA();
}

//Write multiple bytes to uart tx buffer
void USART_WriteString(const char* data) {
    TM_BUFFER_Write(&TX_BUFFER, (uint8_t *)data, strlen(data));
    check_TX_DMA();
}

uint8_t USART_ReadByte(uint8_t* byte) {
    if (TM_USART_BufferCount(USARTx)) {
        *byte = TM_USART_Getc(USARTx);
        return 1;
    }
    return 0;
}

/*************************/
/* Handle DMA interrupts */
/*************************/
void TM_DMA_TransferCompleteHandler(DMA_Stream_TypeDef* stream) {
    /* Check if transfer complete stream was our stream */
    if (stream == TM_USART_DMA_GetStreamTX(USARTx)) {
        TX_BUFFER.Out += txDMAsize;
        /* Do we have still anything to send? */
        check_TX_DMA();
    }
}
    
