/* Includes ------------------------------------------------------------------*/
#include "flash_if.h"

/* Watchdog handle */
static
IWDG_HandleTypeDef hiwdg;

/**
 * \brief           Initializes and starts independent watchdog
 * \return          0 on success, non-zero on failure
 */
uint8_t
iwdg_init(void) {    
    hiwdg.Instance = IWDG;                  /* Watchdog structure */

    /* IWDG setup */
    /**
     * clk = LSI / prescaler = 32k / 256 = 128Hz
     * time = reload / clk = 1024 / 128 = 8 seconds
     */
    hiwdg.Init.Prescaler = IWDG_PRESCALER_256;
    hiwdg.Init.Reload = 1024;
    
    HAL_IWDG_Init(&hiwdg);                  /* Init watchdog */
    
    return 0;
}

/**
 * \brief       Reloads watchdog counter
 * \note        Must be called periodically to prevent device to run to reset state
 * \retval      1: Watchdog not reloaded
 * \retval      0: Watchdog reloaded
 */
uint8_t
iwdg_reload(void) {
    __HAL_IWDG_RELOAD_COUNTER(&hiwdg);      /* Reload watchdog counter */
    return 0;
}

