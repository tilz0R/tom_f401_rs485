from kivy.app import App
from kivy.uix.floatlayout   import FloatLayout
from kivy.lang import Builder
from kivy.clock import Clock
from kivy.uix.screenmanager import ScreenManager, Screen, NoTransition
from kivy.factory import Factory
from kivy.properties import ObjectProperty
from kivy.uix.popup import Popup
from kivy.uix.label import Label
from kivy.properties import ListProperty
from kivy.properties import StringProperty
from kivy.uix.gridlayout import GridLayout
from kivy.properties import NumericPropertys
#import RPi.GPIO as GPIO
#import MFRC522
import signal
import serial

class LabelB(Label):
  bcolor = ListProperty([1,1,1,1])

Factory.register('KivyB', module='LabelB')

class Boxes(FloatLayout):
    fillSwitch = StringProperty()
    OTSwitch = StringProperty()
    pSwitch = StringProperty()
    temperature = StringProperty()

    port = serial.Serial(
        None,
        baudrate=115200,
        parity=serial.PARITY_NONE,
        stopbits=serial.STOPBITS_ONE,
        bytesize=serial.EIGHTBITS,
        timeout=.5)

    def __init__(self, **kwargs):
        super(Boxes, self).__init__(**kwargs)
        #self.temperature = self.GET_status()
        Clock.schedule_interval(self.GET_status, 1.0)

    def GET_status(self, dt):
        return
        self.port.write('\xb5\x01\x04\x09\x00\x00\x00\xa9')
        result = self.port.readline()
        packet = [elem.encode("hex") for elem in result]
        print(packet.__sizeof__())
        if(packet.__sizeof__() > 50):
            print (int(packet[0],16))
            #only display the status packet, not a normal response packet
            if(int(packet[0],16) == 169):
                fillSwitch = int(packet[1], 16)
                OTSwitch = int(packet[2], 16)
                pSwitch = int(packet[3], 16)
                temperature = (int(packet[4], 16) * 256) + (int(packet[5], 16))

                if(fillSwitch == 0):
                    self.ids.fillSw_lbl.text = 'Fill Low'
                    self.ids.fillSw_lbl.bcolor = 1, 0, 0, 1
                    self.ids.fillSw_lbl.color = 1, 1, 1, 1
                else:
                    self.ids.fillSw_lbl.text = 'Fill High'
                    self.ids.fillSw_lbl.bcolor = 0, 1, 0, 1
                    self.ids.fillSw_lbl.color = 0, 0, 0, 1

                if(OTSwitch == 1):
                    self.ids.OTSw_lbl.text = 'Over Temp'
                    self.ids.OTSw_lbl.bcolor = 1, 0, 0, 1
                    self.ids.OTSw_lbl.color = 1, 1, 1, 1
                else:
                    self.ids.OTSw_lbl.text = 'Under Temp'
                    self.ids.OTSw_lbl.bcolor = 0, 1, 0, 1
                    self.ids.OTSw_lbl.color = 0, 0, 0, 1

                if(pSwitch == 1):
                    self.ids.pSw_lbl.text = 'No Pressure'
                    self.ids.pSw_lbl.bcolor = 1, 0, 0, 1
                    self.ids.pSw_lbl.color = 1, 1, 1, 1
                else:
                    self.ids.pSw_lbl.text = 'Pressure OK'
                    self.ids.pSw_lbl.bcolor = 0, 1, 0, 1
                    self.ids.pSw_lbl.color = 0,0,0,1

                # self.ids.OTSw_lbl.text = str(OTSwitch)
                # self.ids.pSw_lbl.text = str(pSwitch)
                self.ids.temp_lbl.text = str(temperature)
        #return str(temperature)

    def Pump1_FAST(self):
        self.port.write('\xb5\x01\x04\x01\x02\x00\x00\xa9')

    def Pump1_SLOW(self):
        self.port.write('\xb5\x01\x04\x01\x01\x01\x00\xa9')

    def Pump1_OFF(self):
        self.port.write('\xb5\x01\x04\x01\x00\x00\x00\xa9')

    def Pump2_FAST(self):
        self.port.write('\xb5\x01\x04\x02\x02\x00\x00\xa9')

    def Pump2_SLOW(self):
        self.port.write('\xb5\x01\x04\x02\x01\x01\x00\xa9')

    def Pump2_OFF(self):
        self.port.write('\xb5\x01\x04\x02\x00\x00\x00\xa9')

    def Heater1_ON(self):
        self.port.write('\xb5\x01\x04\x04\x01\x01\x00\xa9')

    def Heater1_OFF(self):
        self.port.write('\xb5\x01\x04\x04\x01\x00\x00\xa9')

    def Heater2_ON(self):
        self.port.write('\xb5\x01\x04\x04\x02\x01\x00\xa9')

    def Heater2_OFF(self):
        self.port.write('\xb5\x01\x04\x04\x02\x00\x00\xa9')

    def Lights_ON(self):
        self.port.write('\xb5\x01\x04\x03\x01\x00\x00\xa9')

    def Lights_OFF(self):
        self.port.write('\xb5\x01\x04\x03\x00\x00\x00\xa9')

    def Dump_ON(self):
        self.port.write('\xb5\x01\x04\x06\x01\x00\x00\xa9')

    def Dump_OFF(self):
        self.port.write('\xb5\x01\x04\x06\x00\x00\x00\xa9')

    def Fill_ON(self):
        self.port.write('\xb5\x01\x04\x07\x01\x00\x00\xa9')

    def Fill_OFF(self):
        self.port.write('\xb5\x01\x04\x07\x00\x00\x00\xa9')

    def HeatEx_ON(self):
        self.port.write('\xb5\x01\x04\x08\x01\x00\x00\xa9')

    def HeatEx_OFF(self):
        self.port.write('\xb5\x01\x04\x08\x00\x00\x00\xa9')

    def Relay1_ON(self):
        self.port.write('\xb5\x01\x04\x05\x01\x01\x00\xa9')

    def Relay1_OFF(self):
        self.port.write('\xb5\x01\x04\x05\x01\x00\x00\xa9')

    def Relay2_ON(self):
        self.port.write('\xb5\x01\x04\x05\x02\x01\x00\xa9')

    def Relay2_OFF(self):
        self.port.write('\xb5\x01\x04\x05\x02\x00\x00\xa9')

    def Pump1Pop(self):
        content = Pump1Popup(text="Choose what to do with the recording")
        content.bind(on_pump1_answer=self._on_pump1_answer)
        self.popup = Popup(title="Pump 1 Options",
                           content=content,
                           size_hint=(None, None),
                           size=(480, 400),
                           auto_dismiss=False)
        self.popup.open()

    def Pump2Pop(self):
        content = Pump2Popup(text="Choose what to do with the recording")
        content.bind(on_pump2_answer=self._on_pump2_answer)
        self.popup = Popup(title="Pump 2 Options",
                           content=content,
                           size_hint=(None, None),
                           size=(480, 400),
                           auto_dismiss=False)
        self.popup.open()

###################################################################################
# button_callbacks
###################################################################################
    def _on_pump1_answer(self, instance, answer):
        #print ("USER ANSWER: ", repr(answer))
        if (answer == 'off'):
            self.Pump1_OFF()
            self.ids.pump1_lbl.text = 'Pump1 OFF'
            self.ids.pump1_lbl.state = 'normal'

        if (answer == 'slow'):
            self.Pump1_SLOW()
            self.ids.pump1_lbl.text = 'Pump1 SLOW'
            self.ids.pump1_lbl.state = 'down'

        if (answer == 'fast'):
            self.Pump1_FAST()
            self.ids.pump1_lbl.text = 'Pump1 FAST'
            self.ids.pump1_lbl.state = 'down'

        self.popup.dismiss()

    def _on_pump2_answer(self, instance, answer):
        #print ("USER ANSWER: ", repr(answer))
        if (answer == 'off'):
            self.Pump2_OFF()
            self.ids.pump2_lbl.text = 'Pump2 OFF'
            self.ids.pump2_lbl.state = 'normal'

        if (answer == 'slow'):
            self.Pump2_SLOW()
            self.ids.pump2_lbl.text = 'Pump2 SLOW'
            self.ids.pump2_lbl.state = 'down'

        if (answer == 'fast'):
            self.Pump2_FAST()
            self.ids.pump2_lbl.text = 'Pump2 FAST'
            self.ids.pump2_lbl.state = 'down'

        self.popup.dismiss()

###################################################################################
# popups
###################################################################################
class Pump1Popup(GridLayout):
    text = StringProperty()
    def __init__(self, **kwargs):
        self.register_event_type('on_pump1_answer')
        super(Pump1Popup, self).__init__(**kwargs)

    def on_pump1_answer(self, *args):
        pass

class Pump2Popup(GridLayout):
    text = StringProperty()
    def __init__(self, **kwargs):
        self.register_event_type('on_pump2_answer')
        super(Pump2Popup, self).__init__(**kwargs)

    def on_pump2_answer(self, *args):
        pass

presentation = Builder.load_file("test_layout.kv")

class TestApp(App):
    def build(self):
        return Boxes()

if __name__ == '__main__':
    TestApp().run()