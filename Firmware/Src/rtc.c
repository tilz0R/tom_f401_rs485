#include "rtc.h"

#define RTC_MAGIC 0xAACCEE55

static
RTC_HandleTypeDef RtcHandle;

/**
 * \brief           Initialize RTC peripheral
 * \return          0 on success, non-zero on failure
 */
uint8_t
rtc_init(void) {
    uint32_t status;
    HAL_StatusTypeDef response;
    RCC_OscInitTypeDef RCC_OscInitStruct;
    RCC_PeriphCLKInitTypeDef  PeriphClkInitStruct;

    /* Enable LSI oscillator */
    RCC_OscInitStruct.OscillatorType =  RCC_OSCILLATORTYPE_LSI;
    RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
    RCC_OscInitStruct.LSIState = RCC_LSI_ON;
    RCC_OscInitStruct.LSEState = RCC_LSE_OFF;
    if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK) {
        __NOP();
    }

    /* Set source for RTC to LSI */
    PeriphClkInitStruct.PeriphClockSelection = RCC_PERIPHCLK_RTC;
    PeriphClkInitStruct.RTCClockSelection = RCC_RTCCLKSOURCE_LSI;
    if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInitStruct) != HAL_OK) {
        __NOP();
    }
    
    /* Configure RTC setup */
    RtcHandle.Instance            = RTC;
    RtcHandle.Init.HourFormat     = RTC_HOURFORMAT_24;
    RtcHandle.Init.AsynchPrediv   = RTC_ASYNCH_PREDIV;
    RtcHandle.Init.SynchPrediv    = RTC_SYNCH_PREDIV;
    RtcHandle.Init.OutPut         = RTC_OUTPUT_DISABLE;
    RtcHandle.Init.OutPutPolarity = RTC_OUTPUT_POLARITY_HIGH;
    RtcHandle.Init.OutPutType     = RTC_OUTPUT_TYPE_OPENDRAIN;

    __HAL_RCC_PWR_CLK_ENABLE();                 /* Enable PWR peripheral clock */
    __HAL_RCC_RTC_ENABLE(); 
    HAL_PWR_EnableBkUpAccess();                 /* Allow access to BKP Domain */
    status = HAL_RTCEx_BKUPRead(&RtcHandle, RTC_BKP_DR1);   /* Get RTC status */

    if (status == RTC_MAGIC) {                  /* Check if RTC already initialized */

    } else {
        response = HAL_RTC_Init(&RtcHandle);    /* Try to init RTC */
        if (response != HAL_OK) {
            return 1;                           /* Error initializing RTC */
        }
        
        /* Save data to backup regiser */
        HAL_RTCEx_BKUPWrite(&RtcHandle, RTC_BKP_DR1, RTC_MAGIC); 
    }
    
    return 0;
}

/**
 * \brief           Set the backup registers for bootloader
 * \retval          0: Program should stay in boot
 * \retval          1: Backup registers are not valid, jump can be performed
 */
uint8_t
rtc_read_boot(void) {
    /* read first and second register */
    uint32_t reg = HAL_RTCEx_BKUPRead(&RtcHandle, RTC_BKP_DR2);

    if (reg == 0x01) {
        return 0;
    }

    return 1;
}

/**
 * \brief           Reset the used backup registers to 0x00
 * \return          0 on success, non-zero on failure
 */
uint8_t
rtc_clear_boot(void) {
    HAL_RTCEx_BKUPWrite(&RtcHandle, RTC_BKP_DR2, 0); 

    return 0;
}

/**
 * \brief           Set the backup registers for bootloader
 * \return          0 on success, non-zero on failure
 */
uint8_t
rtc_set_boot(void) {
    HAL_RTCEx_BKUPWrite(&RtcHandle, RTC_BKP_DR2, 0x01);
    
    return 0;
}
