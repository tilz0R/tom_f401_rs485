import rs485_commands
from serial_controller import NoResponseException, ErrorPacketException
import struct
import time

FIRMWARE_STATUS_PING =           0x01
FIRMWARE_STATUS_JUMP_TO_BOOT =   0x02
FIRMWARE_STATUS_BOOTLOADER =     0x03
FIRMWARE_STATUS_DETAILS =        0x04
FIRMWARE_STATUS_ERASE_FLASH =    0x05
FIRMWARE_STATUS_SEND_FW_DATA =   0x06
FIRMWARE_STATUS_UPDATE_PERCENTAGE = 0x07

FIRMWARE_ERROR_UNKNOWN =         0x01
FIRMWARE_ERROR_NO_RESPONSE =     0x02
FIRMWARE_ERROR_JUMP_TO_BOOT =    0x03
FIRMWARE_ERROR_PARSE =           0x04
FIRMWARE_ERROR_SW_OLDER =        0x05
FIRMWARE_ERROR_HARDWARE =        0x06
FIRMWARE_ERROR_HARDWARE_TYPE =   0x07
FIRMWARE_ERROR_ERASE_ERROR =     0x08
FIRMWARE_ERROR_CRC_FILE =        0x09
FIRMWARE_ERROR_FLASH =           0x10
FIRMWARE_ERROR_JUMP_TO_MAIN =    0x11

def error_to_str(error):
    if error == FIRMWARE_ERROR_UNKNOWN: return "Unknown error"
    if error == FIRMWARE_ERROR_NO_RESPONSE: return "No response from device"
    if error == FIRMWARE_ERROR_JUMP_TO_BOOT: return "Jump to boot error"
    if error == FIRMWARE_ERROR_PARSE: return "Command parse error"
    if error == FIRMWARE_ERROR_SW_OLDER: return "select software has lower or the same version"
    if error == FIRMWARE_ERROR_HARDWARE: return "Software hardware id error"
    if error == FIRMWARE_ERROR_HARDWARE_TYPE: return "Software hardware type error"
    if error == FIRMWARE_ERROR_ERASE_ERROR: return "Chip erase error"
    if error == FIRMWARE_ERROR_CRC_FILE: return "CRC error"
    if error == FIRMWARE_ERROR_FLASH: return "Flash errr"
    if error == FIRMWARE_ERROR_JUMP_TO_MAIN: return "Jump to main error"


def status_to_str(status):
    if status.status == FIRMWARE_STATUS_PING: return "Pinging device"
    if status.status == FIRMWARE_STATUS_JUMP_TO_BOOT: return "Jumping to boot"
    if status.status == FIRMWARE_STATUS_BOOTLOADER: return "We are in bootloader"
    if status.status == FIRMWARE_STATUS_DETAILS: return "Sending details command"
    if status.status == FIRMWARE_STATUS_ERASE_FLASH: return "Erasing flash"
    if status.status == FIRMWARE_STATUS_SEND_FW_DATA: return "Sending fw data"
    if status.status == FIRMWARE_STATUS_UPDATE_PERCENTAGE: return "Update percentage - " + str(status.data)


class FirmwareStatus:
    def __init__(self, status, data):
        self.status = status
        self.data = data

class FirmwareFile(Exception):
    pass

class FirmwareController:

    def __init__(self, serial_controller, filename, address):
        self._driver = serial_controller

        # read file contents to RAM
        try:
            contents = b''
            with open(filename, mode='rb') as file:
                contents = file.read()

            magic, header_version, header_size, file_size, hardware_type, software_major, software_minor, hardware = struct.unpack("<IIIIIIII", contents[0:32])

            self._software_major = software_major
            self._software_minor = software_minor
            self._hardware = hardware
            self._hardware_type = hardware_type
            self._binary_size = file_size
            self._data = contents[32:]

        except Exception as e:
            raise FirmwareFile(str(e))

        if magic != 0xC55CAA55:
            raise FirmwareFile("Invalid magic number")

        self._address = address

        self._callback = None
    def set_callback(self, callback):
        self._callback = callback

    def start(self):

        # get device details
        response = self.check_valid_type()
        if not response:
            return response

        # ping device
        self.send_status_report(FIRMWARE_STATUS_PING)
        try:
            response = self.send_command(rs485_commands.PC_CMD_STATUS)

        except NoResponseException:
            return FIRMWARE_ERROR_NO_RESPONSE
        except ErrorPacketException:
            return FIRMWARE_ERROR_UNKNOWN

        # parse response
        status = rs485_commands.parse_status_command(response)

        if status == rs485_commands.STATUS_IDLE:
            # we are in idle. Jump to boot
            status = self.jump_to_boot()

            if status != 0:
                return FIRMWARE_ERROR_JUMP_TO_BOOT
    
        self.send_status_report(FIRMWARE_STATUS_BOOTLOADER)

        # erase flash
        self.send_status_report(FIRMWARE_STATUS_ERASE_FLASH)
        response = self.erase_flash()
        if response != 0:
            return FIRMWARE_ERROR_ERASE_ERROR

        # send fw fata
        self.send_status_report(FIRMWARE_STATUS_SEND_FW_DATA)
        response = self.send_fw_data()

        if response != 0:
            return FIRMWARE_ERROR_FLASH

        # jump to main
        response = self.jump_to_main()

        if response != 0:
            return FIRMWARE_ERROR_JUMP_TO_MAIN

        return 0

    def send_fw_data(self):

        # send data
        try:
            packet_counter = 1
            data_remaining = self._binary_size
            counter = 0
            percentage = 0

            while counter != self._binary_size:
                data_to_send = data_remaining
                if data_to_send > 248:
                    data_to_send = 248

                self.send_command(rs485_commands.PC_CMD_FW_DATA, struct.pack("<I", packet_counter) + self._data[counter:counter + data_to_send]);
                counter += data_to_send
                data_remaining -= data_to_send
                packet_counter += 1

                new_percentage = int(counter / float(self._binary_size) * 100)

                if new_percentage != percentage:
                    self.send_status_report(FIRMWARE_STATUS_UPDATE_PERCENTAGE, new_percentage)
                    percentage = new_percentage

        except NoResponseException:
            return FIRMWARE_ERROR_NO_RESPONSE
        except ErrorPacketException:
            return FIRMWARE_ERROR_UNKNOWN

        # send end. This will check crc
        try:
            self.send_command(rs485_commands.PC_CMD_FW_END, struct.pack("<II", self._software_major, self._software_minor));
        except NoResponseException:
            return FIRMWARE_ERROR_NO_RESPONSE
        except ErrorPacketException:
            # crc calculation error, should retry
            return FIRMWARE_ERROR_CRC_FILE

        return 0

    def erase_flash(self):
        try:
            size = struct.pack("<I", self._binary_size)

            response = self.send_command(rs485_commands.PC_CMD_ERASE, size)
        except NoResponseException:
            return FIRMWARE_ERROR_NO_RESPONSE
        except ErrorPacketException:
            return FIRMWARE_ERROR_UNKNOWN

        return 0

    def jump_to_main(self):
        try:
            response = self.send_command(rs485_commands.PC_CMD_JUMP_TO_MAIN)
        except NoResponseException:
            return FIRMWARE_ERROR_NO_RESPONSE
        except ErrorPacketException:
            return FIRMWARE_ERROR_UNKNOWN

        return 0


    def check_valid_type(self):
        self.send_status_report(FIRMWARE_STATUS_DETAILS)

        try:
            response = self.send_command(rs485_commands.PC_CMD_DETAILS)

            details = rs485_commands.parse_details_command(response)

            if not details:
                return FIRMWARE_ERROR_PARSE

            if self._software_major < details.sw_major:
                #return FIRMWARE_ERROR_SW_OLDER
                pass

            if self._software_major == details.sw_major:
                if self._software_minor <= details.sw_minor:
                    #return FIRMWARE_ERROR_SW_OLDER
                    pass

            if self._hardware_type != details.hw_type:
                return FIRMWARE_ERROR_HARDWARE_TYPE

            if self._hardware != details.hw:
                return FIRMWARE_ERROR_HARDWARE

        except NoResponseException:
            return FIRMWARE_ERROR_NO_RESPONSE
        except ErrorPacketException:
            return FIRMWARE_ERROR_UNKNOWN
        except rs485_commands.ParseException:
            return FIRMWARE_ERROR_PARSE

        return 0

    def jump_to_boot(self):
        
        # send the device to bootloader
        self.send_status_report(FIRMWARE_STATUS_JUMP_TO_BOOT)
        try:
            response = self.send_command(rs485_commands.PC_CMD_JUMP_TO_BOOT)
        except NoResponseException:
            return FIRMWARE_ERROR_NO_RESPONSE
        except ErrorPacketException:
            return FIRMWARE_ERROR_UNKNOWN

        # wait for device to be in bootloader
        max_tries = 5
        attempts = 0
        success = False
        while attempts < max_tries:

            try:
                response = self.send_command(rs485_commands.PC_CMD_STATUS)

                status = rs485_commands.parse_status_command(response)

                if status == rs485_commands.STATUS_BOOTLOADER:
                    success = True
                    break
            except NoResponseException:
                pass
            except ErrorPacketException:
                pass

            if success == True:
                break

            time.sleep(1)
            attempts += 1

        # did we fail to get to bootloader
        if success == False:
            return FIRMWARE_ERROR_JUMP_TO_BOOT

        return 0

    def send_status_report(self, status, data = None):
        if self._callback:
            self._callback(FirmwareStatus(status, data))

    def send_command(self, cmd, data = None):
        return self._driver.send_receive(self._address, cmd, data)