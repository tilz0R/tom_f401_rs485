/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __COMMON_COMMANDS_H
#define __COMMON_COMMANDS_H

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Bootloader and application addressed
 */
#define BOOTLOADER_ADDR             0x08000000UL
#define APPLICATION_ADDR            0x08008000UL
    
/* Code when application is valid in flash */
#define APPLICATION_VALID           0x00000001UL
     
/* Response values for STATUS command */
#define STATUS_BOOTLOADER           0x00000002UL
#define STATUS_IDLE                 0x00000001UL
    
/* Important includes for both bootloader and main program */
#include "stdint.h"
#include "packet.h"
#include "iwdg.h"
#include "rtc.h"
#include "crc.h"
#include "bootloader.h"
#include "application.h"
#include "flash_if.h"
     
uint8_t common_init(void);

#ifdef __cplusplus
}
#endif

#endif /* __PACKET_H */
