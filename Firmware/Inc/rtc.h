#ifndef RTC_H
#define RTC_H

#ifdef __cplusplus
extern "C" {
#endif
     
#include "stm32f4xx_hal.h"
#include "stm32f4xx_hal_rtc.h"
#include "stm32f4xx_hal_rtc_ex.h"
#include <stdint.h>

#define RTC_ASYNCH_PREDIV    0x7F
#define RTC_SYNCH_PREDIV     0x0130

uint8_t rtc_read_boot(void);
uint8_t rtc_clear_boot(void);
uint8_t rtc_set_boot(void);
uint8_t rtc_init(void);

#ifdef __cplusplus
}
#endif
     
#endif
