import argparse
import crcmod
import os
import pprint
import random
import re
import struct
import sys
import time

from comm.STMCRC import crc32_stm32 as crc32

HEADERSIZE = 16

MAGIC = 0xC55CAA55
HEADER_VERSION = 1
debug=0

parser = argparse.ArgumentParser(description='Firmware parser')
parser.add_argument('-b','--bindest', required=True, type=str, help='path of the source bin file')
parser.add_argument('-out','--outdest', required=False, type=str, help='path of the parsed bin file')
parser.add_argument('-sw','--software', required=False, type=str, help='Software number of the bin file - default 1.0')
parser.add_argument('-hw','--hardware', required=False, type=int, help='Hardware number of the bin file - default 1')
parser.add_argument('-type', required=True, type=int, help='Hardware type of the bin')
args = parser.parse_args()

if not args.hardware:
    args.hardware = 1;

if not args.software:
    args.software = "1.0"

m = re.search('(\d)\.(\d)', args.software)
if m != None:
    software_major = int(m.group(1))
    software_minor = int(m.group(2))
else:
    print("invalid software version. Required format: <major>.<minor>");
    exit()

hardware_type = args.type

if hardware_type > 16:
    print("invalid hardware type. Maximum number is 16");
    exit() 

hardware = args.hardware

def getHeader():

    header = struct.pack("<IIIIIIII", MAGIC, HEADER_VERSION, 32, binfile_size + 4, hardware_type, software_major, software_minor, hardware)

    #print header
    return bytearray(header)


def OnParse():
    global binfile
    #self.binfile = open(self.path, 'rb')

    path = os.path.dirname(os.path.realpath(__file__)) + os.path.sep


    newbinFile = open(path+"%s%u_%u.lbin" % ("binary", software_major, software_minor) ,"wb")


    newbinFile.write(getHeader())
    newbinFile.write(bytearray(binfile_data))

    crc = crc32(bytearray(binfile_data))
    newbinFile.write(bytearray(struct.pack('I',crc)))

    newbinFile.close()

    if binfile:
        binfile.close()


binfile = open(args.bindest, 'rb');
binfile_data = binfile.read()
binfile_size = len(binfile_data)

#print "File size " + str(binfile_size)

OnParse()

#print "Parse Complete"


