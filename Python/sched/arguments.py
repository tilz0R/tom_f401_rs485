
class ArgumentException(Exception):
    pass


class Argument:
    def __init__(self, name):
        self._name = name
        self._value = None

    def set_value(self, value):
        self._value = self._parse(value)

    def get_value(self):
        return self._value

    def get_name(self):
        return self._name

    def _parse(self, value):
        # Must be implemented in child
        pass


class IntegerArgument(Argument):
    def _parse(self, value):
        try: 
            a = int(value)
            return a
        except ValueError:
            raise ArgumentException("Not an Integer")


class FlowArgument(IntegerArgument):
    def __init__(self):
        IntegerArgument.__init__(self, "flow")

    def _parse(self, value):
        arg = IntegerArgument._parse(self, value)

        if arg > 100 or arg < 0:
            raise ArgumentException("Value does not fit in 0 to 100 range")

        return value


class AmountArgument(IntegerArgument):
    def __init__(self, name):
        IntegerArgument.__init__(self, name)

    def _parse(self, value):
        arg = IntegerArgument._parse(self, value)

        if arg > 100 or arg < 0:
            raise ArgumentException("Value does not fit in 0 to 100 range")

        return value


class ReagentArgument(Argument):
    REAGENTS = {
        "Reagent 1": 1,
        "Reagent 2": 2,
        "Reagent 3": 3,
        "Reagent 4": 4,
        "Reagent 5": 5,
        "Reagent 6": 6,
    }

    def __init__(self, name):
        Argument.__init__(self, name)

    def _parse(self, value):
        if value not in ReagentArgument.REAGENTS:
            raise ArgumentException("Invalid reagent")

        return value

    def get_reagent_index(self):
        if self._value is not None:
            return ReagentArgument.REAGENTS[self._value]

        raise ArgumentException("No value")