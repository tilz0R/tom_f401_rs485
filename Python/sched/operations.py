import collections
import time

"""
        # Arguments must be list
        if arguments == None:
            arguments = []

        # String is also collection this is why we check for that
        if type(arguments) is str:
            raise OperationException("Arguments type error")

        # Should be list by now
        if not isinstance(arguments, collections.Sequence):
            # Create if necessary
            if isinstance(arguments, Argument):
                arguments = [arguments]
            else:
                # Dead end
                raise OperationException("Arguments type error")

        # Set parameter anyway
        self.arguments = arguments
        """
from sched.arguments import Argument, AmountArgument, ReagentArgument


class OperationException(Exception):
    pass


class OperationRuntimeException(Exception):
    pass


class OperationArgumentException(Exception):
    pass

class Operations:

    def __init__(self):
        self._commands = {}

    def add(self, command):
        if not isinstance(command, Operation):
            raise OperationException("Not a valid command")

        for command_name in self._commands:
            if command_name == command.get_name():
                raise OperationException("Command already present")

        self._commands[command.get_name()] = command

    def get_operations(self):
        return self._commands


class Operation:
    def __init__(self, name):
        self._name = name

    def get_arguments(self):
        return None

    def get_name(self):
        return self._name

    def get_argument(self, name, arguments):
        for arg in arguments:
            if name == arg.get_name():
                return arg.get_value()

        raise OperationArgumentException("Missing argument")


class ExceptionOperation(Operation):
    def __init__(self, serial_controller):
        self._controller = serial_controller

        Operation.__init__(self, "exception")

    def execute(self, arguments):
        print "executing operation exception"

        time.sleep(10)

        print "operation executed"
        #raise Exception("Testing exception")

        return False

    def get_arguments(self):
        return None