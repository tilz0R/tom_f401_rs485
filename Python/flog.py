import threading;
import Queue as queue;
import logging
import datetime;
import os;

def flog_debug(description): flog_log(FLog.LEVEL_DEBUG, description)
def flog_info(description): flog_log(FLog.LEVEL_INFO, description)
def flog_warn(description): flog_log(FLog.LEVEL_WARN, description)
def flog_error(description): flog_log(FLog.LEVEL_ERROR, description)

def flog_log(level, description):
    if FLog.log_level >= level:
        item = FLogItem(level, description)
        if FLog.queue:
            FLog.queue.put(item)
        else:
            print(flog_format_output(item))

def flog_set_log_level(level):
    FLog.log_level = level

def flog_format_output(item):
    date = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")

    log_str = "[{0}] {1}: {2}".format(date, level_string(item.level), item.description)

    return log_str

def level_string(level):
    if level == 1: return "ERROR"
    if level == 2: return "WARN"
    if level == 3: return "INFO"
    if level == 4: return "DEBUG" 

class FLogItem:
    def __init__(self, level, description):
        self.level = level
        self.description = description

class FLog(threading.Thread):
    LEVEL_DEBUG = 4
    LEVEL_INFO = 3
    LEVEL_WARN = 2
    LEVEL_ERROR = 1

    queue = None
    _db = None
    log_level = 4

    def __init__(self, logfile):
        threading.Thread.__init__(self)

        FLog.queue = queue.Queue()

        self.daemon = True
        self._running = False
        self.start()
        
        dirname, filename = os.path.split(os.path.abspath(__file__))
        self.log_handle = open(logfile, "w+")

    def stop(self):
        while FLog.queue.qsize():
            pass;
        self._running = False;
        self.join();

    def run(self):

        self._running = True;

        while self._running:
            try:
                item = FLog.queue.get(True, 0.4);
                            
                if item:
                    log_str = flog_format_output(item)
                    self.log_handle.write(log_str+'\r\n')
                    self.log_handle.flush()
                    print(log_str)
            except queue.Empty:
                pass;
