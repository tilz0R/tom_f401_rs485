/**
  ******************************************************************************
  * File Name          : USART.h
  * Description        : This file provides code for the configuration
  *                      of the USART instances.
  ******************************************************************************
  ** This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * COPYRIGHT(c) 2017 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __PACKET_H
#define __PACKET_H

#ifdef __cplusplus
 extern "C" {
#endif

#include "stdint.h"
#include "usart.h"
     
#define PC_RX_BUFFER_SIZE           128 /*!< Buffer size for received non-processed data bytes */
     
#define PC_MAX_PACKET_SIZE          255 /*!< Size for packet data */
     
#define PC_START_BYTE               0xAA/*!< Packet start byte */
#define PC_STOP_BYTE                0x55/*!< Packet stop byte */
     
#define PC_ADDR_BROADCAST           0xFF/*!< Broadcast address */
#define PC_ADDR_MASTER              0x01/*!< Address of master */
     
/* Common commands between main program and bootloader */
#define PC_CMD_STATUS               0x01
#define PC_CMD_WHO_AM_I             0x02
#define PC_CMD_DETAILS              0x03
#define PC_CMD_ERROR                0x04
     
/* Main program commands */
#define PC_CMD_JUMP_TO_BOOT         0x20
     
typedef enum {
    PC_STATE_WAITING,
    PC_STATE_VALID,
    PC_STATE_ERROR
} pc_state_t;
     
typedef struct pc {
    uint8_t sc;                         /*!< State counter */
    uint8_t from;                       /*!< Address from where packet was received */
    uint8_t to;                         /*!< Address where packet should be sent */
    size_t len;                         /*!< Packet data length */
    uint8_t cmd;                        /*!< Packet command value */
    size_t mc;                          /*!< Current message counter */
    uint8_t data[PC_MAX_PACKET_SIZE];   /*!< Buffer for packet data */
    uint16_t crc;                       /*!< CRC value */
    uint8_t cc;                         /*!< CRC counter */
    pc_state_t state;                   /*!< Packet status */
    uint32_t lrt;                       /*!< Time when byte for packet was last time received */
} pc_t;

typedef uint8_t (*pc_cmd_cb_t) (const pc_t* pc);
     
/**
 * \brief           Linked list of commands for packet
 */
typedef struct pc_cmd {
    struct pc_cmd* next;                /*!< Linked list to next entry */
    uint8_t cmd;                        /*!< Command value for packet */
    pc_cmd_cb_t cb;                     /*!< Callback function when command is received */
} pc_cmd_t;

/**
 * \brief           Packet error enumeration
 */
typedef enum pc_error {
    PC_ERROR_CRC,                       /*!< CRC error detected on packet */
    PC_ERROR_INVALID_CMD,               /*!< Packet is for us but command callback is not registered */
} pc_error_t;
     
uint8_t pc_init(void);
uint8_t pc_process(void);
uint8_t pc_cmd_add(uint8_t cmd, pc_cmd_cb_t cb);
uint8_t pc_set_resp_data(const void* data, uint8_t len);

void pc_error_callback(const pc_t* pc, pc_error_t err);
uint8_t pc_getaddr_callback(const pc_t* pc);

#ifdef __cplusplus
}
#endif

#endif /* __PACKET_H */

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
