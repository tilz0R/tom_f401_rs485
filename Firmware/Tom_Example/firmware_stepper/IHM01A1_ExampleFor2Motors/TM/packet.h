#ifndef __PACKET_H
#define __PACKET_H

#include "stdint.h"
#include "stdio.h"
#include "string.h"

#include "usart.h"

typedef struct {
    uint8_t address;
    uint8_t length;
    uint8_t data[255];
    uint8_t counter;
    uint8_t status;
    uint8_t state;
} packet_t;

#define PACKET_STATUS_OK        0x01
#define PACKET_STATUS_ERROR     0x02

#define PACKET_START_BYTE       0xB5
#define PACKET_STOP_BYTE        0xA9

void PACKET_Init(void);
packet_t* PACKET_Read(void);
void PACKET_Write(packet_t* packet);

#endif
