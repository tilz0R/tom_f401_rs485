/* Includes ------------------------------------------------------------------*/
#include "flash_if.h"

#define FLASH_START_ADDR                    0x08000000UL/*!< Start address of flash memory */
#define FLASH_MAGIC_NUM                     0xAACCEE55

#define FLASH_CLEAR_FLAGS()                 __HAL_FLASH_CLEAR_FLAG((FLASH_FLAG_OPERR | FLASH_FLAG_WRPERR | FLASH_FLAG_PGAERR | \
                                                                    FLASH_FLAG_PGPERR | FLASH_FLAG_PGSERR | FLASH_FLAG_RDERR));

/**
 * \brief           Descriptor for flash sector
 */
typedef struct {
    uint8_t num;                            /*!< Sector number */
    uint32_t size;                          /*!< Size of sector in units of bytes */                
} flash_sector_t;

#define FLASH_SECTOR_SIZE_16K               0x00004000UL
#define FLASH_SECTOR_SIZE_64K               (FLASH_SECTOR_SIZE_16K << 2)
#define FLASH_SECTOR_SIZE_128K              (FLASH_SECTOR_SIZE_16K << 3)

/* List of flash sectors */
const flash_sector_t
sectors[] = {
    {FLASH_SECTOR_0, FLASH_SECTOR_SIZE_16K},
    {FLASH_SECTOR_1, FLASH_SECTOR_SIZE_16K},
    {FLASH_SECTOR_2, FLASH_SECTOR_SIZE_16K},
    {FLASH_SECTOR_3, FLASH_SECTOR_SIZE_16K},
    {FLASH_SECTOR_4, FLASH_SECTOR_SIZE_64K},
    {FLASH_SECTOR_5, FLASH_SECTOR_SIZE_128K},
    //{FLASH_SECTOR_6, FLASH_SECTOR_SIZE_128K},
    //{FLASH_SECTOR_7, FLASH_SECTOR_SIZE_128K},
};

/* Flash settings */
flash_settings_t flash_settings;


/**
 * \brief           Get sector pointer to specific address
 * \param[in]       addr: Address in flash to get sector for
 * \return          Pointer to sector structure or NULL on failure
 */
const static
flash_sector_t* get_sector(uint32_t addr) {
    uint32_t curr_addr = FLASH_START_ADDR;
    uint8_t i;
    
    /* Find start sector for flash address */
    for (i = 0; i < sizeof(sectors) / sizeof(sectors[0]); i++) {
        if (addr >= curr_addr && addr < (curr_addr + sectors[i].size)) {
            return &sectors[i];
        }
        curr_addr += sectors[i].size;
    }
    return NULL;
}

/**
 * \brief           Initialize and prepare flash for next operations
 * \param[in,out]   *f: Pointer to \ref flash_t structure with description
 * \return          0 on success, non-zero on failure
 */
uint8_t
flash_init(flash_t* f, uint32_t start_addr, uint32_t len) {
    memset(f, 0x00, sizeof(*f));            /* Reset flash structure */
    f->start_addr = f->addr = start_addr;   /* Set addresses */
    f->total_len = f->rem_len = len;        /* Set length of program */
    
    return 0;
}

/**
 * \brief           Erase flash previously initialized with \ref flash_init
 * \param[in,out]   *f: Pointer to \ref flash_t structure with description
 * \return          0 on success, non-zero on failure
 */
uint8_t
flash_erase(flash_t* f) {
    const flash_sector_t* sector_start;
    const flash_sector_t* sector_end;
    FLASH_EraseInitTypeDef eraseFlash;
    uint32_t sectorError;
    
    sector_start = get_sector(f->start_addr);   /* Get sector number on start of program */
    sector_end = get_sector(f->start_addr + f->total_len);  /* Get sector number on end of program */
    
    if (!sector_start || !sector_end) {     /* Check if sectors exists */
        return 1;                           /* Serious error */
    }
            
    iwdg_reload();                          /* Reload watchdog */
    
    HAL_FLASH_Unlock();                     /* Unlock flash access */
    FLASH_CLEAR_FLAGS();                    /* Clear all flash flags */
    
    /* Do erase procedure */
    eraseFlash.TypeErase = FLASH_TYPEERASE_SECTORS;
    eraseFlash.Banks = FLASH_BANK_1;
    eraseFlash.Sector = sector_start->num;
    eraseFlash.NbSectors = sector_end->num - sector_start->num + 1;
    eraseFlash.VoltageRange = FLASH_VOLTAGE_RANGE_3;
    if ((HAL_FLASHEx_Erase(&eraseFlash, &sectorError)) != HAL_OK) {
        __NOP();
    }
    HAL_FLASH_Lock();                       /* Lock flash access */
    
    return 0;
}

/**
 * \brief           Program data to flash with specific length
 * \note            Address where to save next packet is available in *f structure
 * \param[in,out]   *f: Pointer to \ref flash_t structure with description
 * \param[in]       *data: Pointer to data to program to flash
 * \param[in]       len: Length of data in units of bytes
 * \return          0 on success, non-zero on failure
 */
uint8_t 
flash_program(flash_t* f, const void* data, size_t len) {
    uint32_t blocks;
    const uint8_t* pData = data;
    uint8_t status = 0;
            
    iwdg_reload();                          /* Reload watchdog */
    
    HAL_FLASH_Unlock();                     /* Unlock flash access */
    
    /* Write by size of 4 bytes */
    blocks = len >> 2;                      /* Get number of words to write */
    while (!status && blocks--) {
        FLASH_CLEAR_FLAGS();                /* Clear all flash flags */
        if (HAL_FLASH_Program(FLASH_TYPEPROGRAM_WORD, f->addr, *(volatile uint32_t *)pData) != HAL_OK) {
            status = 1;                     /* Serious error */
            break;
        }
        pData += 4;                         /* Go to next word */
        f->addr += 4;
        f->rem_len -= 4;
    }
    
    /* Write by size of 1 byte */
    blocks = len % 4;
    while (!status && blocks--) {
        FLASH_CLEAR_FLAGS();                /* Clear all flash flags */
        if (HAL_FLASH_Program(FLASH_TYPEPROGRAM_BYTE, f->addr, *pData) != HAL_OK) {
            return 1;                       /* Serious error */
        }
        pData += 1;
        f->addr += 1;
        f->rem_len -= 1;
    }
    HAL_FLASH_Lock();                       /* Lock flash access */
    
    return status;
}

/**
 * \brief           Finish with flash procedure
 * \param[in,out]   *f: Pointer to \ref flash_t structure
 * \return          0 on success, non-zero on failure
 */
uint8_t 
flash_finish(flash_t* f) {
    return 0;
}

/**
 * \brief           Initialize flash settings
 * \return          0 on success, non-zero on failure
 */
uint8_t
flash_settings_init(void) {
    flash_settings_refresh();               /* Refresh data to RAM */
    
    /**
     * Check flash setup if ever first time written 
     * or if main program is OK
     */
    if (flash_settings.head.magic != FLASH_MAGIC_NUM || flash_settings.head.version != FLASH_SETTINGS_VERSION) {
        memset(&flash_settings, 0x00, sizeof(flash_settings));
        flash_settings.head.magic = FLASH_MAGIC_NUM;    /* Set magic number */
        flash_settings.head.version = FLASH_SETTINGS_VERSION;   /* Set flash version */
        flash_settings.head.valid_program = APPLICATION_VALID;  /* By default, set program as valid */
        
        /* Set other values */
        
        flash_settings_update();            /* Update settings back to flash */
    }
    
    return 0;
}

/**
 * \brief           Update settings back to flash
 * \return          0 on success, non-zero on failure
 */
uint8_t
flash_settings_update(void) {
    flash_t f;
    
    flash_init(&f, FLASH_SETTINGS_ADDR, FLASH_SETTINGS_SIZE);   /* Initialize and prepare flash structure */
    flash_erase(&f);                        /* Erase required part of memory */
    flash_program(&f, &flash_settings, FLASH_SETTINGS_SIZE);    /* Program new values for settings */
    flash_finish(&f);                       /* Finish flash operations */
    
    return 0;
}

/**
 * \brief           Refresh settings from flash to settings structure
 * \return          0 on success, non-zero on failure
 */
uint8_t
flash_settings_refresh(void) {
    memcpy(&flash_settings, (void *)FLASH_SETTINGS_ADDR, FLASH_SETTINGS_SIZE);  /* Read flash settings and copy them to application */
    
    return 0;
}
