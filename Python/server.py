from kivy.app import App
from kivy.properties import ObjectProperty, StringProperty, BooleanProperty
from kivy.uix.button import Button
from kivy.event import EventDispatcher
from comm.serial_controller import SerialController
from comm.firmware_controller import FirmwareController, error_to_str, status_to_str
import flog
import sys
import getopt
import parse_config
import threading

from kivy.uix.boxlayout import BoxLayout

from ops.load_operation import LoadOperation
from sched.operation_plan import OperationPlan
from sched.operations import Operations, ExceptionOperation
from sched.scheduler import OperationSchedulerController


class MyEventDispatcher(EventDispatcher):
    def __init__(self, **kwargs):
        self.register_event_type('on_firmware_status')
        super(MyEventDispatcher, self).__init__(**kwargs)

    def firmware_status_dispatch(self, value):
        # when do_something is called, the 'on_test' event will be
        # dispatched with the value
        self.dispatch('on_firmware_status', value)

    def on_firmware_status(self, *args):
        pass

def parse_command_line(argv):
    configfile = 'config.txt'
    try:

        opts, args = getopt.getopt(argv,"hc:",["config="])
    except getopt.GetoptError:
        print('server.py -c <configfile>')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print('server.py -c <configfile>')
            sys.exit()
        elif opt in ("-c", "--config"):
            configfile = arg

    return configfile


class SystemWidget(BoxLayout):

    name = StringProperty()
    system_enabled = StringProperty("Disabled")
    system_state = StringProperty("Stopped")
    system_disabled = BooleanProperty()

    def __init__(self, name, parent):
        super(SystemWidget, self).__init__()
        self.name = name
        self.system_disabled = True
        self._parent = parent
        self.sys = None

        self.bind(system_disabled=self.on_state_change)

    def on_state_change(self, instance, value):
        if not value:
            self.system_enabled = "Enabled"

    def enable_system(self, sys):
        self.system_disabled = False
        self.sys = sys

    def start_system(self):
        self.system_state = "Running"

    def on_restart(self):
        self._parent.restart_system(self.sys)

    def on_continue(self):
        self._parent.continue_system(self.sys)

    def set_error(self, e):
        self.system_state = "Exception: " + e
class RootWidget(BoxLayout):

    title = StringProperty()
    system_widgets = ObjectProperty(None)

    def __init__(self, plan, sc):
        super(RootWidget, self).__init__()

        self.systems = []
        for i in range(8):
            self.systems.append(SystemWidget(str(i+1), self))

        self.display_first_half = True

        self.plan = plan
        self.sc = sc

        for system in plan.get_enabled_systems():
            self.systems[system.get_id() - 1].enable_system(system)
            system.ui = self.systems[system.get_id() - 1]

        self.update()

    def on_next(self):
        self.display_first_half = not self.display_first_half

        self.update()

    def on_start(self):
        self.sc.start()

    def update(self):

        if self.display_first_half:
            self.title = "Systems 1-4"
        else:
            self.title = "Systems 4-8"

        self.system_widgets.clear_widgets()
        if self.display_first_half:
            for sys in self.systems[0:4]:
                self.system_widgets.add_widget(sys)
        else:
            for sys in self.systems[4:]:
                self.system_widgets.add_widget(sys)

    def restart_system(self, sys):
        self.sc.restart_operation(sys)

    def continue_system(self, sys):
        self.sc.continue_operation(sys)

class main(App):
    def __init__(self, configfile):

        App.__init__(self)

        # parse config file
        settings = parse_config.parse(configfile)

        logger = flog.FLog(settings['logfile'])
        flog.flog_set_log_level(settings['loglevel'])
        
        flog.flog_info("Starting main application")
        self.controller = SerialController(settings['port'], settings['baud'], settings['rs485_strategy'], settings['rs485_retries'])
        self.controller.set_state_callback(self.port_state_callback)
        
        self.controller.connect()

        self._event_dispatcher = MyEventDispatcher()

        self._event_dispatcher.bind(on_firmware_status=self.kivy_firmware_callback)

        operations = Operations()

        # add load operation
        load_operation = LoadOperation(self.controller)
        exception_operation = ExceptionOperation(self.controller)
        operations.add(load_operation)
        operations.add(exception_operation)

        self.plan = OperationPlan()
        self.plan.parse_xml(operations, "schema.xml")

        self.scheduler_controller = OperationSchedulerController(self.plan, self.error_cb, self.progress_cb)

    def error_cb(self, sys, e):
        sys.ui.set_error(str(e))

    def progress_cb(self, sys, state, progress):

        if state == "start":
            sys.ui.start_system()

        if state == "progress":
            pass

        print "progress %2.0f%%" % (progress * 100)

    def kivy_firmware_callback(self, event, data):
        print "Firmware status response: ", status_to_str(data)

    def build(self):
        self.root = RootWidget(self.plan, self.scheduler_controller)
        return self.root

    def button_callback(self, instance):
        if self.controller.is_connected():
            t = threading.Thread(target=self.firmware_thread)

            t.start()

    def port_state_callback(self, state):
        flog.flog_info("Serial port state: {0}".format(str(state)))

    def firmware_callback(self, event): 
        self._event_dispatcher.firmware_status_dispatch(event)

    def firmware_thread(self):
        firmware_updater = FirmwareController(self.controller, "binary1_1.lbin", 0x80);
        firmware_updater.set_callback(self.firmware_callback)
        response = firmware_updater.start()

        if response == 0:
            print "device updated successfuly"
        else:
            print "firmware upgrade error: ", error_to_str(response)


if __name__ == "__main__":
    configfile = parse_command_line(sys.argv[1:])
    app = main(configfile)
    app.run()
