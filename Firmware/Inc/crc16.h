#ifndef _CRC16_INCLUDED_
#define _CRC16_INCLUDED_

enum init_t	{ Add = 0, Init = 1 };

unsigned short CalcCrc16(char *msg, unsigned char length);
unsigned short CalcCrc16pt(char c, enum init_t init);

#endif
