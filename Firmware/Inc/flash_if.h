/* Define to prevent recursive inclusion -------------------------------------*/
/**
 * Flash interface file for several purposes:
 *
 * - Settings stored in flash
 * - General write support
 * - General erase support
 */
#ifndef __FLASH_IF_H
#define __FLASH_IF_H

#ifdef __cplusplus
 extern "C" {
#endif

#include "stdint.h"
#include "common.h"
     
/**
 * \brief       Flash structure for erase/write operations
 */
typedef struct {
    uint32_t start_addr;                    /*!< Start address in flash */
    uint32_t addr;                          /*!< Current write address */
    uint32_t total_len;                     /*!< Total length used for erase operation */
    uint32_t rem_len;                       /*!< Remaining length to write */
} flash_t;
     
/**
 * \brief       Flash header with important data
 */     
typedef struct {
    uint32_t magic;                         /*!< Flash magic number indicating flash was at least once written */
    uint32_t version;                       /*!< Current flash version */
    uint32_t hardware;                      /*!< Hardware version */
    uint32_t sw_major;                      /*!< Software major version */
    uint32_t sw_minor;                      /*!< Software minor version */
    uint32_t valid_program;                 /*!< Value indicating program is valid */
} flash_settings_head_t;
     
/**
 * \brief       Main flash structure
 */
typedef struct {
    flash_settings_head_t head;             /*!< Flash header structure */
    uint32_t boot_mode;                     /*!< Value indicating how bootloader should work */
} flash_settings_t ;

/**
 * \brief       Use sector 2 (third sector in a row) of 16k for settings
 */
#define FLASH_SETTINGS_ADDR                 0x08004000  /*!< Start address for flash settings */
#define FLASH_SETTINGS_SIZE                 sizeof(flash_settings_t)    /* Flash settings size */
#define FLASH_SETTINGS_VERSION              1   /*!< Current flash version */

extern flash_settings_t flash_settings;

uint8_t flash_init(flash_t* f, uint32_t start_addr, uint32_t len);
uint8_t flash_erase(flash_t* f);
uint8_t flash_program(flash_t* f, const void* data, size_t len);
uint8_t flash_finish(flash_t* f);

uint8_t flash_settings_init(void);
uint8_t flash_settings_update(void);
uint8_t flash_settings_refresh(void);
     
#ifdef __cplusplus
}
#endif

#endif /* __FLASH_IF_H */
