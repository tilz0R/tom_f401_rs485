/* Includes ------------------------------------------------------------------*/
#include "application.h"

/**
 * \brief           Initializes application user specific software
 * \return          0 on success, non-zero otherwise
 */
uint8_t
app_userinit(void) {
    /* Assign commands for application for device type B */
    
    return 0;
}

/**
 * \brief           Main loop for user application, called from main function:
 * \note            Function must not implement while (1) {} statement
 *                     as this is done from where it is called
 * \code{c}
int main() {
    main_userinit();
    while (1) {
        ...
        ...
        app_userloop();
    }
}
\endcode
 * \return          0 on success, non-zero otherwise
 */
uint8_t
app_userloop(void) {
    return 0;
}
