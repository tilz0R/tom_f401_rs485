/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __BOOTLOADER_H
#define __BOOTLOADER_H

#ifdef __cplusplus
 extern "C" {
#endif

#include "stdint.h"
#include "common.h"
     
/* Bootloader commands */
#define PC_CMD_JUMP_TO_MAIN         0x10
#define PC_CMD_ERASE                0x11
#define PC_CMD_FW_START             0x12
#define PC_CMD_FW_DATA              0x13
#define PC_CMD_FW_END               0x14

uint8_t bootloader_init(void);
uint8_t bootloader_loop(void);
     
#ifdef __cplusplus
}
#endif

#endif /* __BOOTLOADER_H */
